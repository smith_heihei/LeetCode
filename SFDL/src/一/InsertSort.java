package 一;

/**
 * Created by HomorSmith on 2018/8/3.
 */
public class InsertSort {

    public void insertSort(int arr[]){
        for (int i = 1; i < arr.length; i++) {
            int curValue = arr[i];
            int curPos = i-1;
            while (curPos>=0&&curValue<arr[curPos]){
                arr[curPos+1] = arr[curPos];
                curPos--;
            }
            arr[curPos+1] = curValue;
        }
    }

    public static void main(String[] args) {
        int[] ints = {4,3,4,1,2};
        new InsertSort().insertSort(ints);
        for (int i = 0; i < ints.length; i++) {
            System.out.printf(ints[i]+"\t");
        }
    }
}
