package train.tree;

/**
 * Created by HomorSmith on 2018/6/27.
 */
public class 验证二叉搜索树 {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }


    public boolean isValidBST(TreeNode root) {
        if (root==null){
            return false;
        }
        boolean b = root.right.val < root.val;
        return isValidBST(root.left);
    }
}
