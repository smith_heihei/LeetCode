package train.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by HomorSmith on 2018/7/30.
 */
public class N叉树的最大深度 {
    public static class Node {
        public int val;
        public List<Node> children;

        public Node() {
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    }

    ;


    public int maxDepth(Node root) {
        if (root==null){
            return 0;
        }
        if (root.children==null){
            return 1;
        }
        Queue<Node> wrapQueue = new LinkedList<>();
        wrapQueue.offer(root);
        List<List<Node>> allLevel = new ArrayList<>();
        int maxDepth = 0;
        while (!wrapQueue.isEmpty()) {
            int size = wrapQueue.size();
            maxDepth++;
            for (int i = 0; i < size; i++) {
                Node poll = wrapQueue.poll();
                for (int j = 0; j < poll.children.size(); j++) {
                    wrapQueue.offer(poll.children.get(j));
                }
            }
        }
        return maxDepth;
    }
}
