package train.tree;

import java.util.*;

/**
 * @atuhor :eavawu
 * time:2018/7/2
 * todo:
 */
public class 二叉树的层次遍历 {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

//    public void levelOrder(TreeNode root) {
//        List<List<Integer>> levelList = new ArrayList<>();
//        Queue<TreeNode> queue = new LinkedList<>();
//        queue.offer(root);
//        while (!queue.isEmpty()) {
//            TreeNode pop = queue.poll();
//            System.out.printf("pop is = "+pop.val);
//            if (pop.left!=null){
//                queue.offer(pop.left);
//            }
//            if (pop.right!=null){
//                queue.offer(pop.right);
//            }
//        }
//    }


    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> levelList = new ArrayList<>();

        if (root==null){
            return levelList;
        }
        List<Integer> level = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                if (poll==null){
                    continue;
                }
                level.add(poll.val);
                if (poll.left!=null){
                    queue.offer(poll.left);
                }
                if (poll.right!=null){
                    queue.offer(poll.right);
                }
            }
            levelList.add(level);
            level = new ArrayList<>();
        }
        return levelList;
    }



}
