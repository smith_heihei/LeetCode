package train.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by HomorSmith on 2018/7/2.
 */
public class 二叉树的前序遍历 {

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public List<Integer> preorderTraversal(TreeNode head) {
        List<Integer> allNode = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        TreeNode root = head;
        while (root != null || !stack.isEmpty()) {
            while (root != null) {
                allNode.add(root.val);
                stack.push(root);
                root = root.left;
            }
            if (!stack.isEmpty()) {
                root = stack.pop();
                root = root.right;
            }
        }
        return allNode;
    }
}
