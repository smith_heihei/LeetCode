package train.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by HomorSmith on 2018/7/2.
 */
public class 二叉树的中序遍历 {

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public List<Integer> inorderTraversal(TreeNode head) {
        List<Integer> allNode = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        TreeNode root = head;
        while (root != null || !stack.isEmpty()) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            if (!stack.isEmpty()) {
                root = stack.pop();
                allNode.add(root.val);
                root = root.right;
            }
        }
        return allNode;
    }
}
