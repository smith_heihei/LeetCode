package train.tree;

/**
 * Created by HomorSmith on 2018/6/27.
 */
public class 二叉树的最大深度 {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public int maxDepth(TreeNode root) {
        if(root == null)
            return 0;
        System.out.printf("before left = "+root.left.val);
        int leftDepth = maxDepth(root.left);
        System.out.printf("before right = "+root.right.val);
        int rightDepth = maxDepth(root.right);
        System.out.printf("left = "+leftDepth+"   right = "+rightDepth);
        return leftDepth > rightDepth ? (leftDepth + 1) : (rightDepth + 1);
    }

}
