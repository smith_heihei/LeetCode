package train.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by HomorSmith on 2018/7/30.
 */
public class N叉树的后序遍历 {

    public static class Node {
        public int val;
        public List<Node> children;

        public Node() {
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    }

    public List<Integer> maxDepth(Node root) {
        List<Integer> resultList = new ArrayList<>();
        if (root == null) {
            return resultList;
        }
        if (root.children == null) {
            resultList.add(root.val);
            return resultList;
        }
        Queue<Node> wrapQueue = new LinkedList<>();
        wrapQueue.offer(root);
        List<List<Node>> allLevel = new ArrayList<>();
        int maxDepth = 0;
        while (!wrapQueue.isEmpty()) {
            int size = wrapQueue.size();
            maxDepth++;
            List<Node> line = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                Node poll = wrapQueue.poll();
                line.add(poll);
                for (int j = 0; j < poll.children.size(); j++) {
                    wrapQueue.offer(poll.children.get(j));
                }
            }
            allLevel.add(line);
        }

        for (int i = allLevel.size() - 1; i >= 0; i--) {
            List<Node> nodes = allLevel.get(i);
            for (int j = 0; j < nodes.size(); j++) {
                resultList.add(nodes.get(j).val);
            }
        }


        return resultList;
    }
}
