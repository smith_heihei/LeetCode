package train.string;

/**
 * Created by HomorSmith on 2018/6/25.
 */
public class 最长公共前缀 {

    public String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) {
            return "";
        }
        if (strs.length == 1) {
            return strs[0];
        }
        int minLen = strs[0].length();
        for (int i = 0; i < strs.length; i++) {
            if (strs[i].length() < minLen) {
                minLen = strs[i].length();
            }
        }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < minLen; i++) {
            boolean isCommon = true;
            char commonChar = 'A';
            for (int j = 0; j < strs.length - 1; j++) {

                isCommon = strs[j].charAt(i) == strs[j + 1].charAt(i);
                if (!isCommon) {
                    break;
                }
                commonChar = strs[j].charAt(i);
            }
            if (isCommon) {
                builder.append(commonChar);
            } else {
                break;
            }
        }
        return builder.toString();
    }



    public static void main(String[] args) {
        String s = new 最长公共前缀().longestCommonPrefix(new String[]{"flower", "flow", "flight"});
        System.out.printf("result is " + s);
    }

}
