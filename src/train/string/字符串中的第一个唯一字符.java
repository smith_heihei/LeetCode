package train.string;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * @atuhor :eavawu
 * time:2018/6/23
 * todo:
 */
public class 字符串中的第一个唯一字符 {

    public int firstUniqChar(String s) {
        HashMap<Character, Integer> map = new LinkedHashMap<>();
        for (int i = 0; i < s.length(); i++) {
            if (map.containsKey(s.charAt(i))) {
                map.put(s.charAt(i), map.get(s.charAt(i)) + 1);
            } else {
                map.put(s.charAt(i), 1);
            }
        }

//        Set<Map.Entry<Character, Integer>> entries = map.entrySet();
//        char onlyChar = 'a';
//        for (Map.Entry<Character, Integer> entry :
//                entries) {
//            if (entry.getValue() == 1) {
//                onlyChar = entry.getKey();
//            }
//        }

        int index = -1;
        for (int i = 0; i < s.length(); i++) {
            Integer integer = map.get(s.charAt(i));
            if (integer == 1) {
                index = i;
                break;
            }
        }

//        System.out.printf("onley char = "+onlyChar);
        return index;
    }

    public static void main(String[] args) {
        int loveleetcode = new 字符串中的第一个唯一字符().firstUniqChar("loveleetcode");
        System.out.printf("only one = " + loveleetcode);
    }
}
