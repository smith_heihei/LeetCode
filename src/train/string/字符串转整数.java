package train.string;

import java.util.ArrayList;
import java.util.List;

/**
 * @atuhor :eavawu
 * time:2018/6/24
 * todo:
 */
public class 字符串转整数 {

    public int myAtoi(String str) {
        str = str.trim();

        if ("".equals(str)) {
            return 0;
        }

        int sign = 1;
        int beginCount = 0;
        if (str.charAt(0) == '-') {
            sign = -1;
            beginCount++;
        }

        if (str.charAt(0) == '+') {
            beginCount++;
        }


        List<Integer> numList = new ArrayList<>();
        for (int i = beginCount; i < str.length(); i++) {
            if (isNum(str.charAt(i))) {
                //   System.out.println("i ="+i+"   char = "+str.charAt(i));
                int num = str.charAt(i) - '0';
                System.out.println("i =" + i + "   char = " + str.charAt(i) + " num = " + num);

                numList.add(num);
            } else {
                break;
            }
        }

        if (numList.size() == 0) {
            return 0;
        }

        long sum = 0;
        for (int i = 0; i < numList.size(); i++) {
            sum = sum * 10 + numList.get(i);
            if (sum*sign >= Integer.MAX_VALUE) {
                return Integer.MAX_VALUE;
            }
            if (sum*sign <= Integer.MIN_VALUE) {
                return Integer.MIN_VALUE;
            }

        }
        return (int) sum * sign;

    }


    private boolean isNum(char c) {
        return '0' <= c && c <= '9';
    }


    public static void main(String[] args) {
        int i = new 字符串转整数().myAtoi("-91283472332");
        System.out.printf("num is = " + i);
    }

}
