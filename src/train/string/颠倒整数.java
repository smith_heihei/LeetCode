package train.string;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @atuhor :eavawu
 * time:2018/6/23
 * todo:
 */
public class 颠倒整数 {
    public int reverse(int x) {
        if (x == 0) {
            return 0;
        }
        int tempX = Math.abs(x);
        int sign = x > 0 ? 1 : -1;
        List<Integer> units = new ArrayList<>();
        while (tempX > 0) {
            units.add(tempX % 10);
            tempX = tempX / 10;
            //  System.out.printf("temp x " + tempX);
        }
        System.out.printf("list = " + units);
        int zeroCount = 0;
        for (int i = 0; i < units.size(); i++) {
            if (units.get(i) == 0) {
                zeroCount++;
            } else {
                break;
            }
        }
        // System.out.printf("zeroCount = "+zeroCount);

        long reverse = 0;
        for (int i = zeroCount; i < units.size(); i++) {
            int loopCount = units.size() - 1 - i;
            long curNum = units.get(i);
            for (int j = loopCount; j > 0; j--) {
                curNum = curNum * 10;
            }
            reverse = reverse + curNum;
        }
        if (sign * reverse > Integer.MAX_VALUE || sign * reverse < Integer.MIN_VALUE) {
            return 0;
        }
        return (int) (sign * reverse);
    }


    public static void main(String[] args) {
        int reverse = new 颠倒整数().reverse(1534236469);
        System.out.printf("reverse is = " + reverse);
    }
}
