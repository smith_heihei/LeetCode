package train.linkedlist;

/**
 * Created by HomorSmith on 2018/6/26.
 */
public class 反转链表 {
    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    //1 ->  2 -> 3 -> 4
    public ListNode reverseList(ListNode head) {
        if (head==null||head.next == null) {
            return head;
        }
        ListNode pre = null;
        ListNode curNode = head;
        ListNode nextNode = null;
        while (curNode != null) {
            //记录下下一个节点
            nextNode = curNode.next;
            //改变指向
            curNode.next = pre;
            //变量赋值
            pre = curNode;
            curNode = nextNode;
        }
        return pre;
    }
}
