package train.linkedlist;

/**
 * Created by HomorSmith on 2018/6/26.
 */
public class 删除链表的倒数第N个节点 {

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    //快慢指针
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode fastNode = head;
        //快指针先走N步
        for (int i = 0; i < n; i++) {
            fastNode = fastNode.next;
        }
        ListNode slowNode = head;

        //如果此时为空.证明链表长度为n,倒数n个即为头.移除头指针
        if(fastNode == null) {
            head = head.next ;
            return head;
        }

        //慢指针和快指针联动,找到要删除节点的前一个指针.
        while (fastNode.next!=null){
            slowNode = slowNode.next;
            fastNode = fastNode.next;
        }
        //删除
        slowNode.next = slowNode.next.next;
        return head;

    }
}
