package train.linkedlist;

/**
 * @atuhor :eavawu
 * time:2018/7/21
 * todo:
 */
public class 删除排序链表中的重复元素 {


    public ListNode deleteDuplicates(ListNode head) {
        ListNode tempHead = head;
        while (tempHead != null) {
            if (tempHead.next == null) {
                break;
            }
            if (tempHead.val == tempHead.next.val) {
                tempHead.next = tempHead.next.next;
                continue;
            }
            tempHead = tempHead.next;
        }
        return head;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        ListNode tempHead = head;
        for (int i = 1; i < 10; i++) {
            tempHead.next = new ListNode(i);
            tempHead = tempHead.next;
        }
        ListNode listNode = new 删除排序链表中的重复元素().deleteDuplicates(head);
        listNode.printListNode(listNode);


    }
}
