package train.linkedlist;

/**
 * Created by HomorSmith on 2018/6/27.
 */
public class 环形链表 {

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    public boolean hasCycle(ListNode head) {
        if (head==null||head.next == null){
            return false;
        }
        ListNode tempHead = head.next;
        while (tempHead != null){
            if (tempHead.val == head.val){
                return true;
            }
            System.out.printf(tempHead.val+"\t");
            tempHead = tempHead.next;

        }
        return false;
    }
}
