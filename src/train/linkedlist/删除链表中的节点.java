package train.linkedlist;

/**
 * Created by HomorSmith on 2018/6/26.
 */
public class 删除链表中的节点 {


    public ListNode removeElements(ListNode head, int val) {
        while (head != null) {
            if (head.val == val) {
                head = head.next;
            } else {
                break;
            }
        }
        ListNode tempHead = head;
        ListNode pre = tempHead;
        while (tempHead != null) {
            if (tempHead.val == val) {
                pre.next = tempHead.next;
                tempHead = pre;
            }
            pre = tempHead;
            tempHead = tempHead.next;
        }
        return head;
    }


    public static void main(String[] args) {
        ListNode listNode = ListNode.create(new int[]{1, 2, 2, 2,2,2,10});
        ListNode listNode1 = new 删除链表中的节点().removeElements(listNode, 2);


        listNode.printListNode(listNode1);
    }

}
