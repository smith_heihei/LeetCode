package train.linkedlist;

import java.util.List;

/**
 * Created by HomorSmith on 2018/6/26.
 */
public class 合并两个有序链表 {
    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {

        if (l1==null){
            return l2;
        }
        if (l2==null){
            return l1;
        }
        ListNode firstHead = l1;
        ListNode secondHead = l2;
        ListNode newHead = new ListNode(0);
        ListNode head = newHead;
        if (firstHead.val<secondHead.val){
            newHead.val = firstHead.val;
            firstHead = firstHead.next;
        }else{
            newHead.val = secondHead.val;
            secondHead = secondHead.next;
        }

        while (firstHead!=null&&secondHead!=null){
            if (firstHead.val<=secondHead.val){
                newHead.next = firstHead;
                firstHead = firstHead.next;
            }else{
                newHead.next = secondHead;
                secondHead = secondHead.next;
            }
            newHead = newHead.next;
        }

        if (firstHead!=null){
            newHead.next = firstHead;
        }

        if (secondHead!=null){
            newHead.next = secondHead;
        }

        return head;
    }

}
