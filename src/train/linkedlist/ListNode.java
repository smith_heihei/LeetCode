package train.linkedlist;

import java.util.List;

/**
 * @atuhor :eavawu
 * time:2018/7/21
 * todo:
 */
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int x) {
        val = x;
    }


    //1 ->  2 -> 3 -> 4
    public ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode pre = null;
        ListNode curNode = head;
        ListNode nextNode = null;
        while (curNode != null) {
            //记录下下一个节点
            nextNode = curNode.next;
            //改变指向
            curNode.next = pre;
            //变量赋值
            pre = curNode;
            curNode = nextNode;
        }
        return pre;
    }

    public void printListNode(ListNode head) {
        while (head != null) {
            System.out.printf(head.val + "");
            head = head.next;
        }
    }


    public static ListNode create(int[] data) {
        ListNode head = new ListNode(data[0]);
        ListNode tempHead = head;
        for (int i = 1; i < data.length; i++) {
            tempHead.next = new ListNode(data[i]);
            tempHead = tempHead.next;
        }
        return head;

    }


}
