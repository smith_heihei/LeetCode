package train.linkedlist;

/**
 * Created by HomorSmith on 2018/6/26.
 */
public class 回文链表 {
    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }


    //1 ->  2 -> 3 -> 4
    public ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode pre = null;
        ListNode curNode = head;
        ListNode nextNode = null;
        while (curNode != null) {
            //记录下下一个节点
            nextNode = curNode.next;
            //改变指向
            curNode.next = pre;
            //变量赋值
            pre = curNode;
            curNode = nextNode;
        }
        return pre;
    }

    public boolean isPalindrome(ListNode head) {
        if (head==null){
            return true;
        }
        int len = 0;
        ListNode headTemp = head;
        while (headTemp != null) {
            headTemp = headTemp.next;
            len++;
        }
        headTemp = head;
        for (int i = 0; i < len / 2 + len % 2; i++) {
            headTemp = headTemp.next;
        }

        ListNode fast = headTemp;
        ListNode slow = head;
        ListNode newHead = fast;
        fast = reverseList(fast);
        boolean isPalindrome = true;
        while (fast != null) {
            if (fast.val != slow.val) {
                isPalindrome = false;
                break;
            }
            fast = fast.next;
            slow = slow.next;
        }
        return isPalindrome;
    }

}
