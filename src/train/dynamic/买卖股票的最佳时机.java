package train.dynamic;

/**
 * Created by HomorSmith on 2018/7/12.
 */
public class 买卖股票的最佳时机 {


    public int maxProfit2(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int zhuanMoney = 0;
        int lastBuyPrice = prices[0];
        for (int i = 0; i < prices.length; i++) {
            int todayPrice = prices[i];
            if (todayPrice > lastBuyPrice) {
                zhuanMoney = todayPrice - lastBuyPrice;
            } else {
                lastBuyPrice = todayPrice;
            }
        }
        return zhuanMoney;
    }


    public int maxProfit(int[] prices) {
        int money = 0;
        for (int i = 0; i < prices.length; i++) {
            for (int j = i + 1; j < prices.length; j++) {
                if (prices[j] > prices[i]) {
                    int curOffset = prices[j] - prices[i];
                    if (curOffset > money) {
                        money = curOffset;
                    }
                }
            }
        }
        return money;
    }

    public static void main(String[] args) {
        int i = new 买卖股票的最佳时机().maxProfit(new int[]{7, 1, 5, 3, 6, 4});
        System.out.println("max = " + i);
    }
}
