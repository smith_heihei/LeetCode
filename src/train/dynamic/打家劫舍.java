package train.dynamic;

/**
 * Created by HomorSmith on 2018/7/13.
 */
public class 打家劫舍 {
    public int rob(int[] nums) {
        if (nums.length==0){
            return 0;
        }
       return  maxRob(nums,nums.length-1);
    }

    public int maxRob(int[] nums, int i) {

        if (i == 1) {
            return Math.max(nums[0], nums[1]);
        } else if (i == 0) {
            return nums[0];
        } else {
            int a = maxRob(nums, i - 2) + nums[i];
            int b = maxRob(nums, i - 1);
            return Math.max(a, b);
        }
    }
}
