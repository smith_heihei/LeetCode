package train.dynamic;

import java.util.List;

/**
 * Created by HomorSmith on 2018/7/20.
 */
public class AuthorityResp {


    private List<BehaviorsBean> behaviors;

    public List<BehaviorsBean> getBehaviors() {
        return behaviors;
    }

    public void setBehaviors(List<BehaviorsBean> behaviors) {
        this.behaviors = behaviors;
    }

    public static class BehaviorsBean {
        /**
         * emotion : {"answerEmotionId":0}
         * exception : 获取授权码异常,请稍后再试
         * globalId : 53895824308142091
         * intent : {"appKey":"os.use.authorize","code":1000025,"operateState":1010,"parameters":{"data":{"useSec":7201,"warrantId":"5b51939d632ba93f390c9c37457","appId":"t171"},"examId":"4f44154b5ac78506e139ee6100824ba1"},"type":"function"}
         * results : [{"resultType":"text","values":{"text":"正确获取授权码"}}]
         * sequences : [{"service":"intent"}]
         */

        private EmotionBean emotion;
        private String exception;
        private long globalId;
        private IntentBean intent;
        private List<ResultsBean> results;
        private List<SequencesBean> sequences;

        public EmotionBean getEmotion() {
            return emotion;
        }

        public void setEmotion(EmotionBean emotion) {
            this.emotion = emotion;
        }

        public String getException() {
            return exception;
        }

        public void setException(String exception) {
            this.exception = exception;
        }

        public long getGlobalId() {
            return globalId;
        }

        public void setGlobalId(long globalId) {
            this.globalId = globalId;
        }

        public IntentBean getIntent() {
            return intent;
        }

        public void setIntent(IntentBean intent) {
            this.intent = intent;
        }

        public List<ResultsBean> getResults() {
            return results;
        }

        public void setResults(List<ResultsBean> results) {
            this.results = results;
        }

        public List<SequencesBean> getSequences() {
            return sequences;
        }

        public void setSequences(List<SequencesBean> sequences) {
            this.sequences = sequences;
        }

        public static class EmotionBean {
            /**
             * answerEmotionId : 0
             */

            private int answerEmotionId;

            public int getAnswerEmotionId() {
                return answerEmotionId;
            }

            public void setAnswerEmotionId(int answerEmotionId) {
                this.answerEmotionId = answerEmotionId;
            }
        }

        public static class IntentBean {
            /**
             * appKey : os.use.authorize
             * code : 1000025
             * operateState : 1010
             * parameters : {"data":{"useSec":7201,"warrantId":"5b51939d632ba93f390c9c37457","appId":"t171"},"examId":"4f44154b5ac78506e139ee6100824ba1"}
             * type : function
             */

            private String appKey;
            private int code;
            private int operateState;
            private ParametersBean parameters;
            private String type;

            public String getAppKey() {
                return appKey;
            }

            public void setAppKey(String appKey) {
                this.appKey = appKey;
            }

            public int getCode() {
                return code;
            }

            public void setCode(int code) {
                this.code = code;
            }

            public int getOperateState() {
                return operateState;
            }

            public void setOperateState(int operateState) {
                this.operateState = operateState;
            }

            public ParametersBean getParameters() {
                return parameters;
            }

            public void setParameters(ParametersBean parameters) {
                this.parameters = parameters;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public static class ParametersBean {
                /**
                 * data : {"useSec":7201,"warrantId":"5b51939d632ba93f390c9c37457","appId":"t171"}
                 * examId : 4f44154b5ac78506e139ee6100824ba1
                 */

                private DataBean data;
                private String examId;

                public DataBean getData() {
                    return data;
                }

                public void setData(DataBean data) {
                    this.data = data;
                }

                public String getExamId() {
                    return examId;
                }

                public void setExamId(String examId) {
                    this.examId = examId;
                }

                public static class DataBean {
                    /**
                     * useSec : 7201
                     * warrantId : 5b51939d632ba93f390c9c37457
                     * appId : t171
                     */

                    private int useSec;
                    private String warrantId;
                    private String appId;

                    public int getUseSec() {
                        return useSec;
                    }

                    public void setUseSec(int useSec) {
                        this.useSec = useSec;
                    }

                    public String getWarrantId() {
                        return warrantId;
                    }

                    public void setWarrantId(String warrantId) {
                        this.warrantId = warrantId;
                    }

                    public String getAppId() {
                        return appId;
                    }

                    public void setAppId(String appId) {
                        this.appId = appId;
                    }
                }
            }
        }

        public static class ResultsBean {
            /**
             * resultType : text
             * values : {"text":"正确获取授权码"}
             */

            private String resultType;
            private ValuesBean values;

            public String getResultType() {
                return resultType;
            }

            public void setResultType(String resultType) {
                this.resultType = resultType;
            }

            public ValuesBean getValues() {
                return values;
            }

            public void setValues(ValuesBean values) {
                this.values = values;
            }

            public static class ValuesBean {
                /**
                 * text : 正确获取授权码
                 */

                private String text;

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }
            }
        }

        public static class SequencesBean {
            /**
             * service : intent
             */

            private String service;

            public String getService() {
                return service;
            }

            public void setService(String service) {
                this.service = service;
            }
        }
    }
}
