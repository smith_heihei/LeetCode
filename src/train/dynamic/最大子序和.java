package train.dynamic;

import java.util.Date;

/**
 * Created by HomorSmith on 2018/7/9.
 */
public class 最大子序和 {

    public int maxSubArray(int[] nums) {
        int max = nums[0];
        int cur = nums[0];
        for (int i = 0; i < nums.length; i++) {
            cur = 0;
            for (int j = i; j < nums.length; j++) {
                cur = cur + nums[j];
                if (cur > max) {
                    max = cur;
                }
            }
        }
        return max;
    }


    int maxSubArray2(int[] nums) {
        int ThisSum, MaxSum;
        int i;
        ThisSum = MaxSum = 0;
        for (i = 0; i < nums.length; i++) {
            ThisSum += nums[i]; /* 向右累加 */
            if (ThisSum > MaxSum)
                MaxSum = ThisSum; /* 发现更大和则更新当前结果 */
            else if (ThisSum <  MaxSum&&ThisSum<=0) /* 如果当前子列和为负 */
                ThisSum = nums[i]; /* 则不可能使后面的部分和增大，抛弃之 */
            else{

            }
        }
        return MaxSum;
    }


    public static void main(String[] args) {
        int i = new 最大子序和().maxSubArray2(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4});
        System.out.printf("max = " + i);


    }

}
