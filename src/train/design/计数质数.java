package train.design;

/**
 * @atuhor :eavawu
 * time:2018/7/22
 * todo:
 */
public class 计数质数 {
    public int countPrimes(int n) {
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (isPrime(n)) {
                count++;
            }
        }
        return count;
    }

    boolean isPrime(int a) {
        boolean flag = true;

        if (a < 2) {// 素数不小于2
            return false;
        } else {

            for (int i = 2; i <= Math.sqrt(a); i++) {

                if (a % i == 0) {// 若能被整除，则说明不是素数，返回false

                    flag = false;
                    break;// 跳出循环
                }
            }
        }
        return flag;
    }
}
