package train.design;

import java.util.Stack;

/**
 * @atuhor :eavawu
 * time:2018/7/22
 * todo:
 */
public class MinStack {
    /**
     * initialize your data structure here.
     */
    Stack<Integer> container = new Stack<>();
    int min = Integer.MIN_VALUE;

    public MinStack() {

    }

    public void push(int x) {
        container.push(x);
        getMinNum();
    }

    public void pop() {
        container.pop();
        getMinNum();

    }

    private void getMinNum() {
        min = top();
        for (int i = 0; i < container.size(); i++) {
            if (min > container.get(i)) {
                min = container.get(i);
            }
        }
    }

    public int top() {
        if (container.isEmpty()) {
            return -1;
        }
        return container.peek();
    }

    public int getMin() {
        return min;
    }


    public static void main(String[] args) {
        MinStack minStack = new MinStack();
        minStack.push(-2);
        minStack.push(2);
        minStack.push(-1);

        minStack.pop();


    }
}
