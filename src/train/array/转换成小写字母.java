package train.array;

/**
 * Created by HomorSmith on 2018/7/30.
 */
public class 转换成小写字母 {
    public String toLowerCase(String str) {
        StringBuilder stringBuilder = new StringBuilder(str);
        for (int i = 0; i < stringBuilder.length(); i++) {
            if (stringBuilder.charAt(i) >= 'A' && stringBuilder.charAt(i) < 'Z') {
                stringBuilder.replace(i, 1 + i, ((char)(stringBuilder.charAt(i) + 32))+"");
            }
        }
        return stringBuilder.toString();
    }


    public static void main(String[] args) {
        String toLowerCase = new 转换成小写字母().toLowerCase("Hello");
        System.out.printf("low = "+toLowerCase);
    }
}
