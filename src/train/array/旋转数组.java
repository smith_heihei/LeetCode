package train.array;

/**
 * @atuhor :eavawu
 * time:2018/6/16
 * todo:
 */
public class 旋转数组 {
    public void rotate(int[] nums, int k) {
        for (int i = 0; i < k; i++) {
            int index = nums.length - 2;
            int lastVal = nums[nums.length - 1];
            while (index >=0) {
                nums[index + 1] = nums[index];
                index--;
            }
            nums[0] = lastVal;
           // printArr(nums);
        }
    }

    public static void main(String[] args) {
        int[] ints = {1, 2, 3, 4, 5};
        new 旋转数组().rotate(ints,2);
        printArr(ints);
    }

    private static void printArr(int[] ints) {
        for (int i = 0; i < ints.length; i++) {
            System.out.printf(ints[i]+"\t");
        }
        System.out.println();
    }
}
