package train.array;

/**
 * @atuhor :eavawu
 * time:2018/6/15
 * todo:
 */
public class 买卖股票的最佳时机_II {
    public int maxProfit(int[] prices) {
        int count = 0;
        for (int i = 0; i < prices.length-1; i++) {
            if (prices[i]<prices[i+1]){
                count += prices[i+1] - prices[i];
            }
        }
        return count;
    }
}
