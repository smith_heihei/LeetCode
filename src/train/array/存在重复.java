package train.array;

import java.util.HashSet;
import java.util.Set;

/**
 * @atuhor :eavawu
 * time:2018/6/16
 * todo:
 */
public class 存在重复 {
    public boolean containsDuplicate1(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            //i+1前面的i个肯定不会重复
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] == nums[j]) {
                    return true;
                }
            }
        }
        return false;
    }


    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            set.add(nums[i]);
        }
        return nums.length > set.size();
    }


    public static void main(String[] args) {
        boolean b = new 存在重复().containsDuplicate(new int[]{1, 2, 3, 4, 5});
        System.out.println(b+"");
    }


}
