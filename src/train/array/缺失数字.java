package train.array;

/**
 * Created by HomorSmith on 2018/7/30.
 */
public class 缺失数字 {
    public int missingNumber(int[] nums) {
        int[] numsCopy = new int[nums.length + 1];
        for (int i = 0; i < nums.length; i++) {
            int val = nums[i];
            numsCopy[val] = val;
        }


        int val = 0;
        for (int i = 0; i < numsCopy.length - 1; i++) {
            if (numsCopy[i] + 1 != numsCopy[i + 1]) {
                val = numsCopy[i]+1;
                break;
            }
        }
        return val;
    }


    public int missingNumberBest(int[] nums) {
        int sum=0;
        int n=nums.length;
        for(int i=0;i<n;i++){
            sum+=nums[i];
        }
        return ((1+n)*n)/2-sum;
    }

    public static void main(String[] args) {
        int i = new 缺失数字().missingNumber(new int[]{3,0,1});
        System.out.printf("result is = " + i);
    }
}
