package train.array;

/**
 * @atuhor :eavawu
 * time:2018/6/16
 * todo:
 */
public class 只出现一次的数字 {
    public int singleNumber(int[] nums) {
        int totalResult = 0;
        for (int i = 0; i < nums.length; i++) {
            totalResult = totalResult ^ nums[i];
        }
        return totalResult;
    }
}
