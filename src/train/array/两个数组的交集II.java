package train.array;

import java.util.*;

/**
 * @atuhor :eavawu
 * time:2018/6/16
 * todo:
 */
public class 两个数组的交集II {
    public int[] intersect1(int[] nums1, int[] nums2) {
        List<Integer> commonList = new ArrayList<>();
        int index =0;
        for (int i = 0; i < nums1.length; i++) {
            boolean hasCommon = false;
            index = i;
            for (int j = 0; j < nums2.length; j++) {

                if (nums1[index] == nums2[j]&&index <= nums1.length-1) {
                    commonList.add(nums1[index]);
                    System.out.printf("index = "+index+"   j = "+j);
                    index++;
                    hasCommon = true;
                } else {
                    if (hasCommon) {
                        break;
                    }
                }

            }
            if (hasCommon) {
                break;
            }
        }
        Integer[] integers = commonList.toArray(new Integer[commonList.size()]);
        int[] ints = new int[integers.length];
        for (int i = 0; i < integers.length; i++) {
            ints[i] = integers[i];
        }
        return ints;
    }



    public int[] intersect(int[] nums1, int[] nums2) {
        List<Integer> commonList = new ArrayList<>();

        Map<Integer,Integer> map = new HashMap<>();
        for (int i = 0; i < nums1.length; i++) {
            if (map.containsKey(nums1[i])){
                map.put(nums1[i],map.get(nums1[i])+1);
            }else{
                map.put(nums1[i],1);
            }
        }


        for (int i = 0; i < nums2.length; i++) {
            if (map.containsKey(nums2[i])){
                commonList.add(nums2[i]);
                int curCount = map.get(nums2[i]) - 1;
            //    System.out.println("curCount = "+ curCount+"   num = "+nums2[i]);
                map.put(nums2[i],curCount);
                if (curCount==0){
                    map.remove(nums2[i]);
                }
            }
        }
        Integer[] integers = commonList.toArray(new Integer[commonList.size()]);
        int[] ints = new int[integers.length];
        for (int i = 0; i < integers.length; i++) {
            ints[i] = integers[i];
        }
        return ints;

    }


    private static void printArr(int[] ints) {
        for (int i = 0; i < ints.length; i++) {
            System.out.printf(ints[i]+"\t");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] intersect = new 两个数组的交集II().intersect(new int[]{1,2}, new int[]{1,1});
        printArr(intersect);
    }
}
