package train.array;

/**
 * @atuhor :eavawu
 * time:2018/6/15
 * todo:
 */
public class 从排序数组中删除重复项_1 {
    public int removeDuplicates(int[] nums) {
        int cur = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[cur] != nums[i]) {
                cur++;
                nums[cur] = nums[i];
            }
        }
        return cur+1;
    }

    public static void main(String[] args) {
        int i = new 从排序数组中删除重复项_1().removeDuplicates(new int[]{1, 2, 2, 3, 3, 8, 8, 8, 4, 4});
        System.out.println(i);

    }
}
