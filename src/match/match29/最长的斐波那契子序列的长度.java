package match.match29;

/**
 * @atuhor :eavawu
 * time:2018/7/22
 * todo:
 */
public class 最长的斐波那契子序列的长度 {
    public int lenLongestFibSubseq1(int[] A) {
        if (A.length < 3) {
            return 0;
        }
        int max = A[A.length - 1];
        int count = 0;
        int maxSeqCount = 0;
        int f = 1;
        int s = 1;
        int sum = 1;
        for (int i = 0; i <= max; i++) {
            System.out.println("sum = " + sum);
            if (isContain(A, sum)) {
                count++;
                if (count > maxSeqCount) {
                    maxSeqCount = count;
                }
                System.out.println("count = " + count);
            } else {
                count = 0;
            }
            s = sum;
            sum = f + s;
            f = s;
            if (sum > max) {
                break;
            }
        }
        if (maxSeqCount < 3) {
            maxSeqCount = 0;
        }
        return maxSeqCount;
    }


    public int lenLongestFibSubseq(int[] A) {
        if (A.length < 3) {
            return 0;
        }
        int max = A[A.length - 1];
        int count = 0;
        int maxSeqCount = 0;
        boolean hasFib = false;

//        1,3,4,7,11

        for (int i = A.length - 1; i >= 1; i--) {
            int f = A[i];
            count = 0;
            for (int j = i - 1; j >= 0; j--) {
                int s = A[j];
                for (int k = j - 1; k >= 0; k--) {
                    int t = A[k];
                    if (f - s == A[k]) {
                        System.out.println("f = " + f + "   s = " + s + "   t = " + t);
                        f = s;
                        s = t;
                        count++;
                        if (count > maxSeqCount) {
                            maxSeqCount = count;
                        }
                        break;
                    }
                }
            }

        }
        return maxSeqCount+2;
    }


    public boolean isContain(int[] A, int curFib) {
        for (int i = 0; i < A.length; i++) {
            System.out.println("A[" + i + "]" + A[i] + "  curFib = " + curFib);
            if (A[i] == curFib) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int i = new 最长的斐波那契子序列的长度().lenLongestFibSubseq(new int[]{1,2,3,4,5,6,7,8});
        System.out.printf("long  = " + i);
    }
}
