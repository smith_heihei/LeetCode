package middle;

/**
 * @atuhor :eavawu
 * time:2018/6/6
 * todo:
 */
public class 两数相加 {


    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }


    //反转的方式实现
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        l1 = reverListNode(l1);
        l2 = reverListNode(l2);
       // printList(l1);
       //  printList(l2);
        int l1Len = getLen(l1);
        int l2Len = getLen(l2);
        ListNode longList = l1Len >= l2Len ? l1 : l2;
        ListNode shortList = l2Len <= l1Len ? l2 : l1;
        ListNode longTemp = longList;
        ListNode shortTemp = shortList;
        int shi = 0;
        while (longTemp != null) {
            int shortVal = 0;
            if (shortTemp != null) {
                shortVal = shortTemp.val;
            }
            int sum = longTemp.val + shortVal + shi;
            shi = 0;
            if (sum >= 10) {
                shi = 1;
                longTemp.val = sum % 10;
                if (longTemp.next == null) {
                    longTemp.next = new ListNode(shi);
                    longTemp.next.next = null;
                    break;
                }
            } else {
                longTemp.val = sum;
            }

            if (shortTemp != null) {
                shortTemp = shortTemp.next;
            }
            longTemp = longTemp.next;

        }
        return reverListNode(longList);
    }


    public int getLen(ListNode listNode) {
        ListNode tempHead = listNode;
        int count = 1;
        while (tempHead != null) {
            tempHead = tempHead.next;
            count++;
        }
        return count;

    }

    public ListNode reverListNode(ListNode listNode) {
        ListNode pre = null;
        ListNode cur = listNode;
        ListNode next = cur.next;
        while (cur != null) {
            next = cur.next;
            //指向当前节点的下一个节点指向前一个节点
            cur.next = pre;
            pre = cur;
            cur = next;
        }
        listNode.next = null;
        return pre;

    }

    public void printList(ListNode l) {
        ListNode tempHead = l;
        while (tempHead != null) {
            System.out.printf(tempHead.val + "\t");
            tempHead = tempHead.next;
        }
        System.out.println();
    }

    public static void main(String[] args) {
        ListNode listNode = new ListNode(5);
        ListNode head = listNode;
//        int[] intArr = new int[]{2,4,3};
//        for (int i = 0; i < intArr.length; i++) {
//            listNode.next = new ListNode(intArr[i]);
//            listNode = listNode.next;
//        }


        ListNode secondListNode = new ListNode(5);
        ListNode secondHead = secondListNode;
//        int[] intArr1 = new int[]{6,4};
//        for (int i = 0; i < intArr1.length; i++) {
//            secondHead.next = new ListNode(intArr1[i]);
//            secondHead = secondHead.next;
//        }
        两数相加 两数相加 = new 两数相加();
        ListNode resultListNode = 两数相加.addTwoNumbers(head, secondListNode);
        两数相加.printList(resultListNode);
    }
}
