package middle;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @atuhor :eavawu
 * time:2018/7/5
 * todo:
 */
public class 填充同一层的兄弟节点 {

    public class TreeLinkNode {
        int val;
        TreeLinkNode left, right, next;

        TreeLinkNode(int x) {
            val = x;
        }
    }


    public void connect(TreeLinkNode root) {
        Queue<TreeLinkNode> queue = new LinkedList<>();
        List<TreeLinkNode> level = new ArrayList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            level = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                TreeLinkNode poll = queue.poll();
                if (poll == null) {
                    continue;
                }
                level.add(poll);
                if (poll.left != null) {
                    queue.offer(poll.left);
                }
                if (poll.right != null) {
                    queue.offer(poll.right);
                }
            }
            if (level.size() > 0) {
                return;
            }
            TreeLinkNode header = level.get(0);
            for (int i = 0; i < level.size() - 1; i++) {
                header.next = level.get(i + 1);
                header = header.next;
            }
            header.next = null;
        }
    }


}
