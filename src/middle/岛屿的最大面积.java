package middle;

/**
 * Created by HomorSmith on 2018/6/8.
 */
public class 岛屿的最大面积 {
    public int maxAreaOfIsland(int[][] grid) {
        int max = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 1) {
                    int count = getCount(grid, i, j);
                    if (count > max) {
                        max = count;
                    }
                } else {
                    continue;
                }
            }
        }
        return 0;
    }

    private int getCount(int[][] grid, int i, int j) {
        int count = 0;
        while (grid[i][j] == 1) {
            //左边跑
            if (grid[i][j - 1] == 1) {
                grid[i][j ] = 0;
                j--;
                count++;
            }

            if (grid[i-1][j ] == 1) {
                grid[i][j] = 0;
                i--;
                count++;
            }

            if (grid[i][j+1 ] == 1) {
                grid[i][j] = 0;
                j++;
                count++;
            }

            if (grid[i+1][j ] == 1) {
                grid[i][j] = 0;
                i++;
            }


            //右边跑
            //上面跑
            //下面跑
        }
        return count;
    }

    private boolean isLand(int[][] grid, int row, int col) {
        return (grid[row][col - 1] == 1 || grid[row - 1][col] == 1 || grid[row][col + 1] == 1 || grid[row + 1][col] == 1);
    }


    ;
}
