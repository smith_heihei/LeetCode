package easy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by HomorSmith on 2018/5/28.
 */
public class 键盘行 {


    public String[] findWords1(String[] words) {
        List<String> lineWords = new ArrayList<>();
        lineWords.add("QWERTYUIOP");
        lineWords.add("ASDFGHJKL");
        lineWords.add("ZXCVBNM");
        List<String> resultList = new ArrayList<>();
        for (int i = 0; i < words.length; i++) {
            String singleWord = words[i];
            System.out.println("singleWord = " + singleWord);
            boolean isInLine = true;
            List<Integer> posList = new ArrayList<>();
            for (int j = 0; j < singleWord.length(); j++) {
                char singleChar = singleWord.charAt(j);
                for (int k = 0; k < lineWords.size(); k++) {
                    String s = lineWords.get(k);
                    System.out.println("line word = " + lineWords.get(k));
                    System.out.println("single char = " + singleChar);
                    if (s.contains((singleChar + "").toLowerCase()) || s.contains((singleChar + "").toUpperCase())) {
                        posList.add(k);
                    }
                }

            }

            int first = posList.get(0);
            System.out.println("posList = " + posList.toString());
            for (int k = 0; k < posList.size(); k++) {
                if (first != posList.get(k)) {
                    isInLine = false;
                    break;
                }
            }
            if (isInLine) {
                resultList.add(singleWord);
            }

        }
        return resultList.toArray(new String[resultList.size()]);


    }


    public String[] findWords2(String[] words) {
        Map<String, String> firstLine = new HashMap<>();
        firstLine.put("Q", "Q");
        firstLine.put("W", "W");
        firstLine.put("E", "E");
        firstLine.put("R", "R");
        firstLine.put("T", "T");
        firstLine.put("Y", "Y");
        firstLine.put("U", "U");
        firstLine.put("I", "I");
        firstLine.put("O", "O");
        firstLine.put("P", "P");


        Map<String, String> secondLine = new HashMap<>();
        secondLine.put("A", "A");
        secondLine.put("S", "S");
        secondLine.put("D", "D");
        secondLine.put("F", "F");
        secondLine.put("G", "G");
        secondLine.put("H", "H");
        secondLine.put("J", "J");
        secondLine.put("K", "K");
        secondLine.put("L", "L");


        Map<String, String> thirdLine = new HashMap<>();
        thirdLine.put("Z", "Z");
        thirdLine.put("X", "X");
        thirdLine.put("C", "C");
        thirdLine.put("V", "V");
        thirdLine.put("B", "B");
        thirdLine.put("N", "N");
        thirdLine.put("M", "M");
        List<String> resultList = new ArrayList<>();
        for (int i = 0; i < words.length; i++) {
            String singleWord = words[i];
            singleWord = singleWord.toUpperCase();
            int one = 0, two = 0, three = 0;
            for (int j = 0; j < singleWord.length(); j++) {
                String singleStr = singleWord.charAt(j) + "";
                boolean firstLineContain = firstLine.containsKey(singleStr);
                boolean secondLineContain = secondLine.containsKey(singleStr);
                boolean thirdContain = thirdLine.containsKey(singleStr);
                if (firstLineContain) {
                    one = 1;
                }
                if (secondLineContain) {
                    two = 1;
                }
                if (thirdContain) {
                    three = 1;
                }
                if (one + two + three > 1) {
                    break;
                }
            }
            if (one + two + three == 1) {
                resultList.add(singleWord);
            }
        }
        return resultList.toArray(new String[resultList.size()]);
    }


    public String[] findWords(String[] words) {
        Map<String, String> firstLine = new HashMap<>();
        firstLine.put("Q", "Q");
        firstLine.put("W", "W");
        firstLine.put("E", "E");
        firstLine.put("R", "R");
        firstLine.put("T", "T");
        firstLine.put("Y", "Y");
        firstLine.put("U", "U");
        firstLine.put("I", "I");
        firstLine.put("O", "O");
        firstLine.put("P", "P");


        Map<String, String> secondLine = new HashMap<>();
        secondLine.put("A", "A");
        secondLine.put("S", "S");
        secondLine.put("D", "D");
        secondLine.put("F", "F");
        secondLine.put("G", "G");
        secondLine.put("H", "H");
        secondLine.put("J", "J");
        secondLine.put("K", "K");
        secondLine.put("L", "L");


        Map<String, String> thirdLine = new HashMap<>();
        thirdLine.put("Z", "Z");
        thirdLine.put("X", "X");
        thirdLine.put("C", "C");
        thirdLine.put("V", "V");
        thirdLine.put("B", "B");
        thirdLine.put("N", "N");
        thirdLine.put("M", "M");
        List<String> resultList = new ArrayList<>();
        for (int i = 0; i < words.length; i++) {
            String singleWord = words[i];
            singleWord = singleWord.toUpperCase();
            String firstChar = singleWord.charAt(0) + "";
            boolean firstLineContain = firstLine.containsKey(firstChar);
            if (firstLineContain) {
                if (judgeSingleLine(firstLine, singleWord)){
                    resultList.add(words[i]);
                    continue;
                }
            }


            boolean secondLineContain = secondLine.containsKey(firstChar);
            if (secondLineContain) {
                if (judgeSingleLine(secondLine, singleWord)){
                    resultList.add(words[i]);
                    continue;
                }
            }
            boolean thirdContain = thirdLine.containsKey(firstChar);
            if (thirdContain) {
                if (judgeSingleLine(thirdLine, singleWord)){
                    resultList.add(words[i]);
                }
            }
        }
        return resultList.toArray(new String[resultList.size()]);
    }

    private boolean judgeSingleLine(Map<String, String> line, String singleWord) {
        for (int j = 0; j < singleWord.length(); j++) {
            if (!line.containsKey(singleWord.charAt(j) + "")) {
                return false;
            }
        }
        return true;
    }


    public static void main(String[] args) {
        键盘行 keyboardLine = new 键盘行();
        String[] srcWords = new String[]{"Hello", "Alaska", "Dad", "Peace"};
        String[] words = keyboardLine.findWords(srcWords);
        for (int i = 0; i < words.length; i++) {
            System.out.print(words[i] + "\t");
        }
    }

}
