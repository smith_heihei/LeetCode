package easy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HomorSmith on 2018/5/10.
 */
public class 数字的补数 {

    public int findComplement(int num) {
        List<Integer> binnaryArr = getBinnaryArr(num);
        binnaryArr = flip(binnaryArr);
        return getRevertNum(binnaryArr);
    }

    public List<Integer> flip(List<Integer> binaryArr) {
        for (int i = 0; i < binaryArr.size(); i++) {
            binaryArr.set(i, binaryArr.get(i) == 1 ? 0 : 1);
        }
        return binaryArr;
    }

    public int getRevertNum(List<Integer> binaryArr) {
        int revertNum = 0;
        for (int i = 0; i < binaryArr.size(); i++) {
            int tempNum = binaryArr.get(i);
            for (int j = binaryArr.size() - i; j > 1; j--) {
                tempNum = tempNum * 2;
            }
            System.out.println("tempNum = "+tempNum);
            revertNum = revertNum + tempNum;
        }
        return revertNum;
    }


    public List<Integer> getBinnaryArr(int src) {
        List<Integer> binaryList = new ArrayList<>();
        while (src != 0) {
            int i = src % 2;
            binaryList.add(0, i);
            src = src / 2;
        }
        return binaryList;
    }

    public static void main(String[] args) {
        数字的补数 shuzi = new 数字的补数();
        System.out.println(shuzi.findComplement(4));
    }

}
