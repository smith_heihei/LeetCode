package easy;

/**
 * Created by HomorSmith on 2018/5/10.
 */
public class 汉明距离 {
    public int hammingDistance(int x, int y) {
        if (x == y) {
            return 0;
        }
        int distance = 0;
        StringBuilder xStr = getBinnaryArr(x);
        StringBuilder yStr = getBinnaryArr(y);

        int maxLen = xStr.length() - yStr.length() >= 0 ? xStr.length() : yStr.length();
        for (int i = 0; i < maxLen; i++) {
            if (i >= xStr.length()) {
                xStr.insert(0,"0");
            }
            if (i >= yStr.length()) {
                yStr.insert(0,"0");
            }
        }


        System.out.println("x = " + xStr.toString());
        System.out.println("y = " + yStr.toString());

        for (int i = 0; i < maxLen; i++) {
            char xc = xStr.charAt(i);
            char xy = yStr.charAt(i);
            if (xc != xy) {
                distance++;
            }
        }
        return distance;
    }


    public StringBuilder getBinnaryArr(int src) {
        StringBuilder stringBuilder = new StringBuilder();
        while (src != 0) {
            int i = src % 2;
            stringBuilder.insert(0,i);
            src = src / 2;
        }
        return stringBuilder;
    }


    public static void main(String[] args) {
        汉明距离 hmDistance = new 汉明距离();
        int i = hmDistance.hammingDistance(1, 4);
        System.out.println(i);

    }
}
