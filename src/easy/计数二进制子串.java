package easy;

/**
 * @atuhor :eavawu
 * time:2018/7/9
 * todo:
 */
public class 计数二进制子串 {
    public int countBinarySubstrings(String s) {
        int preCount = 0;
        int curCount = 1;
        int resultCount = 0;
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) == s.charAt(i - 1)) {
                curCount++;
            } else {
                preCount = curCount;
                curCount = 1;
            }

            if (preCount >= curCount) {
                resultCount++;
            }
        }

        return resultCount;
    }


    public static void main(String[] args) {
        int i = new 计数二进制子串().countBinarySubstrings("00110011");
        System.out.printf("result  = " + i);
    }
}
