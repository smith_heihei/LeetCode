package easy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HomorSmith on 2018/6/7.
 */
public class 杨辉三角II {
    public List<Integer> getRow(int rowIndex) {
        List<List<Integer>> resultList = new ArrayList<>();
        for (int i = 0; i <=rowIndex; i++) {
            List<Integer> tempList = new ArrayList<>();

            for (int j = 0; j <=i; j++) {
                if (j == 0 || i < 1 || j == i) {
                    tempList.add(1);
                } else {
                    tempList.add(resultList.get(i - 1).get(j - 1) + resultList.get(i- 1).get(j));
                }
            }
            resultList.add(tempList);
           // System.out.printf("tempList = "+tempList.toString());

        }
        return resultList.get(resultList.size()-1);
    }

    public static void main(String[] args) {
       List<Integer> generate = new 杨辉三角II().getRow(3);
        System.out.println(generate);

    }
}
