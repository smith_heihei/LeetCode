package easy;

/**
 * Created by HomorSmith on 2018/6/6.
 */
public class 移动零 {
    public void moveZeroes(int[] nums) {
        int moveCount = 0;
        int curIndex = 0;
        for (int i = 0; i < nums.length - moveCount; i++) {
            if (nums[i]!=0){
                nums[curIndex] = nums[i];
                curIndex++;
            }
        }

        for (int i = curIndex; i < nums.length; i++) {
              nums[i] = 0;
        }
    }

    public static void main(String[] args) {
        int[] intArr = new int[]{0, 0,1};
        new 移动零().moveZeroes(intArr);
        System.out.println();
        System.out.println();
        printArr(intArr);
    }

    private static void printArr(int[] intArr) {
        for (int i = 0; i < intArr.length; i++) {
            System.out.printf(intArr[i]+"\t");
        }
        System.out.println();
    }
}
