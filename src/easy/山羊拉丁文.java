package easy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HomorSmith on 2018/6/11.
 */
public class 山羊拉丁文 {
    public String toGoatLatin(String S) {
        StringBuilder builder = new StringBuilder();
        char[] arr = new char[]{'a', 'e', 'i', 'o', 'u'};
        if (S.contains(" ")) {
            String[] split = S.split(" ");
            for (int i = 0; i < split.length; i++) {
                if (i == split.length -1){
                    builder.append(processWord(split[i], i, arr));
                    break;
                }
                builder.append(processWord(split[i], i, arr)).append(" ");
            }
        } else {
            builder.append(processWord(S, 0, arr));
        }
        return builder.toString();
    }

    public String processWord(String word, int pos, char[] arr) {
        StringBuilder builder = new StringBuilder();
        char c = (word.charAt(0) + "").toLowerCase().charAt(0);
        boolean isContainVowel = false;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == c) {
                isContainVowel = true;
                break;
            }
        }

        if (isContainVowel) {
            builder.append(word).append("ma");
        }else{
            builder.append(word.substring(1, word.length())).append(word.charAt(0)).append("ma");
        }


        for (int i = 0; i < pos+1; i++) {
            builder.append("a");
        }
        return builder.toString();

    }


    public static void main(String[] args) {
        String s = new 山羊拉丁文().toGoatLatin("I speak Goat Latin");
        System.out.printf(s);
    }

}
