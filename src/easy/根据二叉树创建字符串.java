package easy;

/**
 * Created by HomorSmith on 2018/6/14.
 */
public class 根据二叉树创建字符串 {
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }


    /* Function to insert data recursively */
    private void insert(TreeNode node, int data) {
        TreeNode newNode = null;
        if (node.left == null) {
            node.left = new TreeNode(data);
            return;
        } else if (node.right == null) {
            node.right = new TreeNode(data);
            return;
        }
        if (node != null) {
            if (data > node.val) {
                insert(node.right, data);
            } else {
                insert(node.left, data);
            }
        }
    }

    public String tree2str(TreeNode t) {
        return "";
    }

    public String tree2str(TreeNode t,String str){
        return "";
    }


    public static void main(String[] args) {
        根据二叉树创建字符串 stringBinary = new 根据二叉树创建字符串();

        TreeNode treeNode = new TreeNode(1);
        TreeNode tempNode = treeNode;
        int[] arr = new int[]{2,3,4};
        for (int i = 0; i < arr.length; i++) {
            stringBinary.insert(tempNode, arr[i]);
        }

    }

}
