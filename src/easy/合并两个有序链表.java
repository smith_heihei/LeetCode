package easy;


/**
 * Created by HomorSmith on 2018/6/11.
 */
public class 合并两个有序链表 {

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1==null){
            return l2;
        }
        if (l2==null){
            return l1;
        }
        ListNode newListNode,tempListNode = null;
        if (l1.val<l2.val){
            newListNode = l1;
            l1 = l1.next;
        }else{
            newListNode = l2;
            l2 = l2.next;
        }
        tempListNode = newListNode;
        while (l1!=null&&l2!=null){
            if (l1.val<l2.val){
                tempListNode.next = l1;
                l1 = l1.next;
            }else{
                tempListNode.next = l2;
                l2 = l2.next;
            }
            tempListNode = tempListNode.next;
        }
        if (l1!=null){
            tempListNode.next = l1;
        }
        if (l2!=null){
            tempListNode.next = l2;
        }

        return newListNode;
    }

    public static void main(String[] args) {
      ListNode listNode = new ListNode(5);
        ListNode head = listNode;
        int[] intArr = new int[]{6,7,8};
        for (int i = 0; i < intArr.length; i++) {
            listNode.next = new ListNode(intArr[i]);
            listNode = listNode.next;
        }


       ListNode secondListNode = new ListNode(5);
       ListNode secondHead = secondListNode;
        int[] intArr1 = new int[]{6,7,8};
        for (int i = 0; i < intArr1.length; i++) {
            secondHead.next = new ListNode(intArr1[i]);
            secondHead = secondHead.next;
        }

        ListNode listNode1 = new 合并两个有序链表().mergeTwoLists(head, secondListNode);
        while (listNode1.next!=null){
            System.out.printf(listNode1.val +"\t");
          listNode1 = listNode1.next;
        }
    }

}
