package easy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by HomorSmith on 2018/5/10.
 */
public class 唯一摩尔斯密码词 {
    String[] pwds = new String[]{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
    String[] letter = new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

    public int uniqueMorseRepresentations(String[] words) {
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < words.length; i++) {
            String translate = translate(words[i]);
            if (map.containsKey(translate)) {
                continue;
            }
            System.out.println(translate);
            map.put(translate, translate);
        }
        return map.size();
    }

    public String translate(String srcWord) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < srcWord.length(); i++) {
            char[] chars = srcWord.toCharArray();
            System.out.println(chars[i] + "    " + (chars[i] - 'a') + "   " + pwds[chars[i] - 'a']);
            builder.append(pwds[chars[i] - 'a']);
        }
        return builder.toString();
    }


    public static void main(String[] args) {
        唯一摩尔斯密码词 mos = new 唯一摩尔斯密码词();
        String[] mosWords = new String[]{"gin", "zen", "gig", "msg"};
        System.out.println(mos.uniqueMorseRepresentations(mosWords));
    }
}
