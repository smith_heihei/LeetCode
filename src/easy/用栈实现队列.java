package easy;

import java.util.Stack;

/**
 * Created by HomorSmith on 2018/7/6.
 */
public class 用栈实现队列 {

    private Stack<Integer> enStack = new Stack<>();
    private Stack<Integer> outStack = new Stack<>();


    /** Initialize your data structure here. */
    public 用栈实现队列() {

    }

    /** Push element x to the back of queue. */
    public void push(int x) {
        outStack.push(x);
    }

    /** Removes the element from in front of queue and returns that element. */
    public int pop() {
        for (int i = 0; i < outStack.size(); i++) {
            enStack.push(outStack.pop());
        }
        int data = enStack.pop();
        for (int i = 0; i < enStack.size(); i++) {
            outStack.push(enStack.pop());
        }
        return data;
    }

    /** Get the front element. */
    public int peek() {
        for (int i = 0; i < outStack.size(); i++) {
            enStack.push(outStack.pop());
        }
        int data = enStack.peek();
        for (int i = 0; i < enStack.size(); i++) {
            outStack.push(enStack.pop());
        }
        return data;
    }

    /** Returns whether the queue is empty. */
    public boolean empty() {
       return outStack.empty();
    }
}
