package easy;

/**
 * Created by HomorSmith on 2018/5/22.
 */
public class 翻转图像 {
    public int[][] flipAndInvertImage(int[][] A) {
        rotate180(A);
        printArr(A);
        flip(A);
        printArr(A);
        return A;
    }

    private void flip(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            int[] ints = a[i];
            for (int j = 0; j < ints.length; j++) {
                ints[j] = ints[j]==1?0:1;
            }
        }
    }

    public void rotate180(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            int[] ints = a[i];
            for (int j = 0; j < ints.length / 2; j++) {
                int temp = ints[j];
                ints[j] = ints[ints.length-1 - j];
                ints[ints.length-1 - j] = temp;
            }
        }
    }


    public void printArr(int[][] arr){
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]+"\t");
            }
            System.out.println();
        }
        System.out.println();
    }




    public static void main(String[] args) {
        翻转图像 rotate = new 翻转图像();
        int[][] arr = new int[][]{{1,1,0},{1,1,0},{1,1,1},{1,0,1}};
        rotate.flipAndInvertImage(arr);

    }

}
