package easy;

/**
 * Created by HomorSmith on 2018/6/11.
 */
public class NumArray {
    public int[] nums;
    public NumArray(int[] nums) {
        this.nums = nums;
    }

    public int sumRange(int i, int j) {
        int sum = 0;
        if (i==j){
            return nums[i];
        }
        if ((j-i+1)%2==0){
            //System.out.printf("ou");
            for (int k  = 0; k <= (j-i)/2 ; k++) {
                sum = sum+nums[j-k]+nums[k+i];
             //   System.out.println("last = "+nums[j-k]+"  begin = "+nums[k+i]);
            }
        }else{
        //    System.out.printf("ji");
            for (int k  = 0; k < (j-i)/2 ; k++) {
                sum = sum+nums[j-k]+nums[k+i];
             //   System.out.println("last = "+nums[j-k]+"  begin = "+nums[k+i]);
            }
            sum =sum+ nums[(j-i)/2+i];
        }

        return sum;
    }

    public static void main(String[] args) {
        NumArray numArray = new NumArray(new int[]{-2,0,3,-5,2,-1});
        int i = numArray.sumRange(2, 5);
        System.out.printf("i = "+i);
    }
}
