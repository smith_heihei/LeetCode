package easy;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

/**
 * Created by HomorSmith on 2018/4/28.
 */
public class 宝石与石头 {
    public int numJewelsInStones(String J, String S) {
        if (S == null || "".equals(S)) {
            return 0;
        }
        if (J == null || "".equals(J)) {
            return 0;
        }
        int diamondCount = 0;
        //查找
        for (int i = 0; i < S.length(); i++) {

            if (J.contains(S.charAt(i) + "")) {
                diamondCount++;
            }
        }
        return diamondCount;

    }

    public static void main(String[] args) {
        宝石与石头 baoshi = new 宝石与石头();
        int i = baoshi.numJewelsInStones("aA", "aAAbbbb");
        System.out.println("diamond count = "+i);
    }

}
