package easy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @atuhor :eavawu
 * time:2018/6/1
 * todo:
 */
public class 罗马数字转整数 {
    public int romanToInt(String s) {
        Map<String, Integer> valueMap = new HashMap<>();
        valueMap.put("I", 1);
        valueMap.put("IV", 4);
        valueMap.put("V", 5);
        valueMap.put("IX", 9);
        valueMap.put("X", 10);
        valueMap.put("XL", 40);
        valueMap.put("L", 50);
        valueMap.put("XC", 90);
        valueMap.put("C", 100);
        valueMap.put("CD", 400);
        valueMap.put("D", 500);
        valueMap.put("CM", 900);
        valueMap.put("M", 1000);
        List<String> allRomanString = new ArrayList<>();

        for (int i = 0; i < s.length(); i++) {
            String curStr = s.charAt(i) + "";
            if (i == s.length()-1){
                allRomanString.add(curStr);
                break;
            }

            String nextStr = s.charAt(i + 1) + "";
            if (valueMap.get(curStr) < valueMap.get(nextStr)) {
                allRomanString.add(curStr + nextStr);
                i++;
            } else {
                allRomanString.add(curStr);
            }
        }

        int result = 0;
        for (int i = 0; i < allRomanString.size(); i++) {
            result = result + valueMap.get(allRomanString.get(i));
        }

        return result;
    }


    public static void main(String[] args) {
        int result = new 罗马数字转整数().romanToInt("LVIII");
        System.out.println("结果 = "+result);
    }

}
