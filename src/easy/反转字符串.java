package easy;

/**
 * Created by HomorSmith on 2018/5/24.
 */
public class 反转字符串 {
    public String reverseString(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        char[] chars = s.toCharArray();
        for (int i = 0; i < s.length() / 2; i++) {
            char tempChar = chars[i];
            chars[i] = chars[s.length() - 1 - i];
            chars[s.length() - 1 - i] = tempChar;
        }
        return stringBuilder.append(chars).toString();
    }

    public static void main(String[] args) {
        反转字符串 reverseString = new 反转字符串();
        System.out.println(reverseString.reverseString("hello"));
    }


}
