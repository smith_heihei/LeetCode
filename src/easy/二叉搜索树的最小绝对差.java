package easy;

import sun.reflect.generics.tree.Tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by HomorSmith on 2018/6/13.
 */
public class 二叉搜索树的最小绝对差 {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    /* Function to insert data recursively */
    private void insert(TreeNode node, int data) {
        TreeNode newNode = null;
        if (node.left == null) {
            node.left = new TreeNode(data);
            return;
        } else if (node.right == null) {
            node.right = new TreeNode(data);
            return;
        }
        if (node != null) {
            if (data > node.val) {
                insert(node.right, data);
            } else {
                insert(node.left, data);
            }
        }
    }

    public int getMinimumDifference(TreeNode root) {
        if (root.left != null) {
            return findMin(root, Math.abs(root.val - root.left.val));
        } else {
            return findMin(root, Math.abs(root.right.val - root.val));
        }
    }

    public int findMin(TreeNode root, int min) {
        if (root == null ) {
            return min;
        }

        int leftOffset = min;
        int rightOffset = min;

        if (root.left!=null){
            leftOffset =   Math.abs(root.val - root.left.val);

        }
        if (root.right!=null){
             rightOffset = Math.abs(root.right.val - root.val);
        }

        System.out.println("leftOffset = " + leftOffset + "  rightOffset = " + rightOffset+"  min = "+min);
        if (leftOffset < rightOffset) {
            if (min > leftOffset) {
                min = leftOffset;
            }
        }

        else if (leftOffset > rightOffset){
            if (min > rightOffset) {
                min = rightOffset;
            }
        }

        System.out.println("min = "+min);
        findMin(root.left, min);
        findMin(root.right, min);
        return  min;
    }





    public void printList(List<List<Integer>> lists){
        for (int i = 0; i < lists.size(); i++) {
            for (int j = 0; j < lists.get(i).size(); j++) {
                System.out.printf(lists.get(i).get(j)+"\t");
            }
            System.out.println();
        }
    }


    public static void main(String[] args) {
        二叉搜索树的最小绝对差 minTreeAbs = new 二叉搜索树的最小绝对差();
        TreeNode treeNode = new TreeNode(1);
        TreeNode tempNode = treeNode;
        int[] arr = new int[]{3,2};
        for (int i = 0; i < arr.length; i++) {
            minTreeAbs.insert(tempNode, arr[i]);
        }



        int minimumDifference = minTreeAbs.getMinimumDifference(treeNode);
        System.out.println("min difference = " + minimumDifference);
    }
}
