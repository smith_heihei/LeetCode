package easy;

/**
 * Created by HomorSmith on 2018/6/7.
 */
public class 检测大写字母 {
    public boolean detectCapitalUse(String word) {
        String toUpperCase = word.toUpperCase();
        String toLowerCase = word.toLowerCase();
        String firstUpCase = (word.charAt(0))+"".toUpperCase()+word.substring(1,word.length()).toLowerCase();
        return  (toUpperCase.equals(word)||toLowerCase.equals(word)||firstUpCase.equals(word));
    }

    public static void main(String[] args) {
        new 检测大写字母().detectCapitalUse("G");
    }
}
