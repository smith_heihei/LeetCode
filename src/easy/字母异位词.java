package easy;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by HomorSmith on 2018/4/28.
 */
public class 字母异位词 {
    public boolean isAnagram(String s, String t) {
        Map<Character,Integer> sMap =  new HashMap<>();
        Map<Character,Integer> tMap =  new HashMap<>();

        if (s==null){
            return false;
        }
        if (t==null){
            return false;
        }
        if (s.equals(t)){
            return true;
        }
        if (s.length()!=t.length()){
            return false;
        }
        for (int i = 0; i < s.length(); i++) {
            if (sMap.get(s.charAt(i))==null){
                sMap.put(s.charAt(i),1);
            }else{
                sMap.put(s.charAt(i),sMap.get(s.charAt(i))+1);
            }


            if (tMap.get(t.charAt(i))==null){
                tMap.put(t.charAt(i),1);
            }else{
                tMap.put(t.charAt(i),tMap.get(t.charAt(i))+1);
            }

        }
        for (int i = 0; i < s.length(); i++) {
            if (tMap.get(s.charAt(i))==null||sMap.get(s.charAt(i))==null){
                return false;
            }
            if (!tMap.get(s.charAt(i)).equals(sMap.get(s.charAt(i)))){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        字母异位词 yiwei = new 字母异位词();
        System.out.println(yiwei.isAnagram("",""));
    }
}
