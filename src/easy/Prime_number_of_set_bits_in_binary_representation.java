package easy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HomorSmith on 2018/6/7.
 */
public class Prime_number_of_set_bits_in_binary_representation {
    public int countPrimeSetBits(int L, int R) {
        int count = 0;
        for (int i = L; i <= R; i++) {
            int bitSetCount = getBitSetCount(i);
            boolean prime = isPrime(bitSetCount);
            if (prime){
                count++;
             // System.out.printf("i = "+bitSetCount+"\t");
            }
        }
        return count;
    }
    public int  getBitSetCount(int param){
        int count = 0;
        List<Integer> binnaryArr = getBinnaryArr(param);
//        System.out.printf(binnaryArr.toString()+"\t");
        for (int i = 0; i < binnaryArr.size(); i++) {
            if (binnaryArr.get(i) == 1){
                count++;
            }
        }
        return count;
    }


    boolean isPrime( int a )
    {
        boolean flag = true;

        if (a < 2) {// 素数不小于2
            return false;
        } else {

            for (int i = 2; i <= Math.sqrt(a); i++) {

                if (a % i == 0) {// 若能被整除，则说明不是素数，返回false

                    flag = false;
                    break;// 跳出循环
                }
            }
        }
        return flag;
}

    public List<Integer> getBinnaryArr(int src) {
        List<Integer> binaryList = new ArrayList<>();
        while (src != 0) {
            int i = src % 2;
            binaryList.add(0,i);
            src = src / 2;
        }
        return binaryList;
    }

    public static void main(String[] args) {
        Prime_number_of_set_bits_in_binary_representation prime_number_of_set_bits_in_binary_representation = new Prime_number_of_set_bits_in_binary_representation();
        int i = prime_number_of_set_bits_in_binary_representation.countPrimeSetBits(244, 269);
        System.out.printf("result = "+i);
    }
}
