package easy;

import java.util.*;

/**
 * Created by HomorSmith on 2018/6/1.
 */
public class 下一个更大元素 {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        Map<Integer, Integer> posMap = new LinkedHashMap<>();
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length; j++) {
                if (nums1[i] == nums2[j]) {
                    posMap.put(j, nums2[j]);
                }
            }
        }

        List<Integer> resultList = new ArrayList<>();
        Set<Map.Entry<Integer, Integer>> entries = posMap.entrySet();
        for (Map.Entry<Integer, Integer> entry : entries) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if (key < nums2.length - 1) {
                int curNum = -1;
                for (int i = key; i < nums2.length - 1; i++) {
                    if (nums2[key] < nums2[i + 1]) {
                        curNum = nums2[i + 1];
                        break;
                    }

                }
                resultList.add(curNum);
            }else{
                resultList.add(-1);
            }
        }
        int[] intArr = new int[resultList.size()];
        for (int i = 0; i < resultList.size(); i++) {
            intArr[i] = resultList.get(i);
        }
        return intArr;
    }

    public static void main(String[] args) {
        int[] src = new int[]{4, 1, 2};
        int[] srcTarget = new int[]{1, 3, 4, 2};


//        int[] src = new int[]{1,3,5,2,4};
//        int[] srcTarget = new int[]{6,5,4,3,2,1,7};
        int[] ints = new 下一个更大元素().nextGreaterElement(src, srcTarget);
        for (int i = 0; i < ints.length; i++) {
            System.out.printf(ints[i] + "\t");
        }
    }


}
