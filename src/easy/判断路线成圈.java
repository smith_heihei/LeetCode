package easy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by HomorSmith on 2018/4/28.
 */
public class 判断路线成圈 {
    public boolean judgeCircle(String moves) {
        if (moves==null||"".equals(moves)){
            return false;
        }
        int leftCount = 0;
        int rightCount = 0;
        int upCount = 0;
        int downCount = 0;
        for (int i = 0; i <moves.length(); i++) {
            if (moves.charAt(i)=='D'){
                downCount++;
            }
            if (moves.charAt(i)=='U'){
                upCount++;
            }
            if (moves.charAt(i)=='L'){
                leftCount++;
            }
            if (moves.charAt(i)=='R'){
                rightCount++;
            }

        }
        if (leftCount==rightCount&&downCount==upCount){
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(new 判断路线成圈().judgeCircle("UD"));
    }
}
