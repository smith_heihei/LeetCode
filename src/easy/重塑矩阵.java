package easy;

import java.util.ArrayList;
import java.util.List;

/**
 * @atuhor :eavawu
 * time:2018/6/3
 * todo:
 */
public class 重塑矩阵 {
    public int[][] matrixReshape(int[][] nums, int r, int c) {
        List<Integer> allShapeList = new ArrayList<>();
        int length = nums.length;
        for (int i = 0; i < length; i++) {
            int[] singleLine = nums[i];
            for (int j = 0; j < singleLine.length; j++) {
                allShapeList.add(nums[i][j]);
            }
        }


        if (r * c != allShapeList.size()) {
            return nums;
        }

        int[][] resultNums = new int[r][c];
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                resultNums[i][j] = allShapeList.get(i * c + j);
            }
        }
        return resultNums;

    }

    public static void main(String[] args) {
        int[][] shape = new int[][]{{1, 2}, {3, 4}};
        int[][] ints = new 重塑矩阵().matrixReshape(shape, 1, 4);
        for (int i = 0; i < ints.length; i++) {
            for (int j = 0; j < ints[i].length; j++) {
                System.out.printf(ints[i][j] + "\t");
            }
        }
    }
}
