package easy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by HomorSmith on 2018/6/15.
 */
public class 两个数组的交集 {

    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> common = new HashSet<>();
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length; j++) {
              if (nums2[j] == nums1[i]){
                  common.add(nums1[i]);
              }
            }
        }
        int[] arr = new int[common.size()];
        Integer[] integers = new Integer[common.size()];
        common.toArray(integers);
        for (int i = 0; i < integers.length; i++) {
            arr[i] = integers[i];
        }
        return arr;
    }

    public static void main(String[] args) {
        int[] arr = {1,2,2,1};
        int[] arr1 = {2,2,};
        new 两个数组的交集().intersection(arr,arr1);
    }


}
