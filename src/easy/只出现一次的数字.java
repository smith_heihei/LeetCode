package easy;

/**
 * Created by HomorSmith on 2018/6/6.
 */
public class 只出现一次的数字 {
    public int singleNumber(int[] nums) {
        int res = 0;
        for (int i : nums) {
            res ^= i;
        }
        return res;
    }

    public static void main(String[] args) {
        int[] intArr = new int[]{4,1,2,1,2};
        int i = new 只出现一次的数字().singleNumber(intArr);
        System.out.printf("num is = "+i);
    }
}
