package easy;

/**
 * Created by HomorSmith on 2018/6/5.
 */
public class 写字符串需要的行数 {
    public int[] numberOfLines1(int[] widths, String S) {
        int length = S.length();
        int value = 0;
        int lineCount = 1;
        for (int i = 0; i < widths.length; i++) {
            if (i < widths.length) {
                value = value + widths[i];
                if (value > 100) {
                    lineCount++;
                    value = value % 100;
                }
                System.out.printf("value = %d", value);
            }
        }

        int[] ints = new int[]{lineCount, value};
        return ints;
    }


    public int[] numberOfLines(int[] widths, String S) {


        int length = S.length();
        int lineCount = 1;
        int lastUnits = 0;
        for (int i = 0; i < length; i++) {
            int pos = S.charAt(i) - 'a' ;
            int curCharValue = widths[pos];
//            System.out.println("curCharValue = "+curCharValue+"  pos = "+pos);
            lastUnits = lastUnits + curCharValue;
            if (lastUnits>100){
                lastUnits = curCharValue;
                lineCount++;
            }

        }
        int[] ints = new int[]{lineCount, lastUnits};
        return ints;

    }


    public static void main(String[] args) {
        int[] intArr = new int[]{10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10};
        String s = "abcdefghijklmnopqrstuvwxyz";

//        int[] intArr = new int[]{4, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
//        String s = "bbbcccdddaaa";

        int[] ints = new 写字符串需要的行数().numberOfLines(intArr, s);
        for (int i = 0; i < ints.length; i++) {
            System.out.printf(ints[i] + "\t");
        }
    }
}
