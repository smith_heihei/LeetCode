package easy;

/**
 * Created by HomorSmith on 2018/8/2.
 */
public class 使用最小花费爬楼梯 {

    public int minCostClimbingStairs(int[] cost) {
        if (cost.length == 1) {
            return cost[0];
        }
        if (cost.length == 2) {
            return Math.min(cost[0], cost[1]);
        }

        int[] resultArr = new int[cost.length + 1];
        resultArr[0] = cost[0];
        resultArr[1] = cost[1];
        for (int i = 2; i < cost.length + 1; i++) {
            int constraint = i == cost.length ? 0 : cost[i];
            resultArr[i] = Math.min(resultArr[i - 1], resultArr[i - 2]) + constraint;
        }
        return resultArr[cost.length];
    }


    public int minCostClimbingStairs1(int[] cost) {
        return minCostClimbingStairs1(cost, cost.length-1 );
    }


    public int minCostClimbingStairs1(int[] cost, int pos) {
        if (pos == 0) {
            return cost[0];
        }
        if (pos == 1) {
            return Math.min(cost[0], cost[1]);
        }
        int select = minCostClimbingStairs1(cost, pos - 1) + cost[pos];
        int unSelect = minCostClimbingStairs1(cost, pos - 2) + cost[pos];
        return Math.min(select, unSelect);
    }


    public static void main(String[] args) {
        int i = new 使用最小花费爬楼梯().minCostClimbingStairs1(new int[]{1, 100, 1, 1, 1, 100, 1, 1, 100, 1 });
        System.out.printf("result is " + i);
    }

}
