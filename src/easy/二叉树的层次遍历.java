package easy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by HomorSmith on 2018/6/13.
 */
public class 二叉树的层次遍历 {
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }


    public List<List<Integer>> levelOrderBottom(TreeNode root) {

        if (root==null){
            return new ArrayList<>();
        }

        Queue<TreeNode> queue = new LinkedList<>();
        List<List<Integer>> tree = new ArrayList<>();
        queue.offer(root);
        while (!queue.isEmpty()){
            int size = queue.size();
            List<Integer> line = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                TreeNode poll = queue.poll();
                line.add(poll.val);
                if (poll.left!=null){
                    queue.offer(poll.left);
                }
                if (poll.right!=null){
                    queue.offer(poll.right);
                }
            }
            tree.add(line);
        }

        List<List<Integer>> resultTree = new ArrayList<>();
        for (int i = tree.size()-1;  i>=0; i--) {
            resultTree.add(tree.get(i));
        }
        return resultTree;
    }


    /* Function to insert data recursively */
    private void insert(TreeNode node, int data)
    {
        TreeNode newNode = null;
        if (node.left==null){
            node.left = new TreeNode(data);
            return;
        }else if (node.right==null){
            node.right = new TreeNode(data);
            return;
        }
        if (node!=null){
            if (data>node.val){
                insert(node.right,data);
            }else{
                insert(node.left,data);
            }
        }
    }

    public void preOrder(TreeNode root){
        if (root!=null){
            System.out.println(root.val+"");
            preOrder(root.left);
            preOrder(root.right);
        }
    }

    public void printList(List<List<Integer>> lists){
        for (int i = 0; i < lists.size(); i++) {
            for (int j = 0; j < lists.get(i).size(); j++) {
                System.out.printf(lists.get(i).get(j)+"\t");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        TreeNode rootNode = new TreeNode(5);
        TreeNode tempRoot = rootNode;
        二叉树的层次遍历 obj = new 二叉树的层次遍历();
        for (int i = 0; i < 10; i++) {
            if (i==5){
                continue;
            }
            obj.insert(tempRoot,i);
        }
         tempRoot = rootNode;

        obj.preOrder(rootNode);


        List<List<Integer>> lists = obj.levelOrderBottom(tempRoot);
        obj.printList(lists);
    }
}
