package easy;

import java.util.*;

/**
 * Created by HomorSmith on 2018/6/4.
 */
public class 分糖果 {

    public int distributeCandies(int[] candies) {
        List<Integer> brother = new ArrayList<>();
        Set<Integer> sister = new HashSet<>();
        for (int i = 0; i < candies.length; i++) {
            sister.add(candies[i]);
        }
        //是否超过一半
        int offset =sister.size() - candies.length/2 ;
        if (offset < 0){
            //重复种类
            return sister.size()  ;
        } else {
            //减去多余的种类
            return sister.size() - offset;
        }
    }

    public static void main(String[] args) {
//        int[] intArr = new int[]{1,1,2,2,3,3};
        int[] intArr = new int[]{1,1,2,3};

        int i = new 分糖果().distributeCandies(intArr);
        System.out.printf("种类 = "+i);
    }

}
