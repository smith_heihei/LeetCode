package easy;

/**
 * Created by HomorSmith on 2018/6/15.
 */
public class 报数 {
    /**
     * @param n the nth
     * @return the nth sequence
     */
    public String countAndSay(int n) {
        // Write your code here

        String s = "1";
        int i = 1;
        while (i < n){
            //进行
            s = count(s);
            i ++;
        }
        return s;
    }

    public String count(String a){

        char[] chars = a.toCharArray();
        //计数
        int num = 1;

        StringBuffer buffer = new StringBuffer();

        for(int i = 1; i < chars.length ; i++){

            if (chars[i-1] == chars[i]){
                num++;
            }else {
                buffer.append(num + String.valueOf(chars[i-1]));
                num = 1;
            }
        }
        //拼接最后一个
        buffer.append(num + String.valueOf(chars[chars.length - 1]));
        return  buffer.toString();
    }
}
