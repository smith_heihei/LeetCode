package easy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by HomorSmith on 2018/6/5.
 */
public class 二叉树的层平均值 {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public List<Double> containsDuplicate(TreeNode root){
        List<Double> result = new ArrayList<Double>();
        Queue<TreeNode> tn = new LinkedList<TreeNode>();

        if(root == null) return null;

        tn.add(root);
        while(!tn.isEmpty()){
            int n = tn.size();
            long sum = 0;
            for(int i = 0; i < n; i++){
                TreeNode a = tn.poll();
                sum += a.val;
                System.out.println(a.val);
                if(a.left!=null) tn.add(a.left);
                if(a.right!=null) tn.add(a.right);
            }
            System.out.println(sum);
            System.out.println(n);
            result.add(((double) (sum))/n);
        }
        return result;
    }

    public static void main(String[] args) {

    }
}
