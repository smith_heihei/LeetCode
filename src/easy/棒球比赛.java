package easy;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

import java.util.Iterator;
import java.util.Stack;

/**
 * @atuhor :eavawu
 * time:2018/6/1
 * todo:
 */
public class 棒球比赛 {
    public int calPoints(String[] ops) {
        Stack<Integer> points = new Stack<>();

        for (int i = 0; i < ops.length; i++) {
            String op = ops[i];
            if (!op.equals("C") && !op.equals("D")&&!op.equals("+")) {
                points.push(Integer.parseInt(op));
                continue;
            }
            if (op.equals("C")) {
                if (!points.isEmpty()) {
                    points.pop();
                }
            }

            if (op.equals("D")) {
                if (!points.isEmpty()) {
                    points.push(points.get(points.size() - 1) * 2);
                }
            }

            if (op.equals("+")) {
                if (points.size() >= 2) {
                    Integer secondLast = points.get(points.size() - 2);
                    Integer firstLast = points.get(points.size() - 1);
                    points.push(secondLast + firstLast);

                }
            }


        }
        Iterator<Integer> iterator = points.iterator();
        int totalPoint = 0;
        while (iterator.hasNext()) {
            int result = iterator.next();
            totalPoint = totalPoint + result;
        }
        return totalPoint;
    }

    public static void main(String[] args) {
        int i = new 棒球比赛().calPoints(new String[]{"5", "2", "C", "D", "+"});
        System.out.println("分数 = " + i);
    }

}
