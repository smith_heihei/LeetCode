package easy;

import java.util.*;

/**
 * @atuhor :eavawu
 * time:2018/5/31
 * todo:
 */
public class 子域名访问计数 {

    public List<String> subdomainVisits(String[] cpdomains) {
        HashMap<String, Integer> resultMap = new HashMap<>();
        List<String> domenSrcList = new ArrayList<>();
        //初始化传入的域名数量
        List<Integer> domanCountList = new ArrayList<>();
        //分散后的所有域名
        List<List<String>> allDomainSpliteList = new ArrayList<>();

        for (int i = 0; i < cpdomains.length; i++) {
            String[] split = cpdomains[i].split(" ");
            domenSrcList.add(split[1]);
            domanCountList.add(Integer.parseInt(split[0]));
        }

//        System.out.println("domenSrcList = "+domenSrcList+" domanCountList = "+domanCountList);


        for (int i = 0; i < domenSrcList.size(); i++) {
            List<String> domainSpliteList = new ArrayList<>();
//            System.out.println("domenSrcList.get("+i+") = "+domenSrcList.get(i));
            String[] splitArr = domenSrcList.get(i).split("\\.");
//            System.out.println("splitArr = "+Arrays.asList(splitArr));
            List<String> spliteList = Arrays.asList(splitArr);
            domainSpliteList.addAll(spliteList);
            allDomainSpliteList.add(domainSpliteList);

//            System.out.println("domainSpliteList = "+domainSpliteList);


        }




        for (int i = 0; i < allDomainSpliteList.size(); i++) {
            List<String> singeDomanList = allDomainSpliteList.get(i);
            Integer singleDomanCount = domanCountList.get(i);
            int size = singeDomanList.size();
            if (size == 2) {
                putOrAdd(resultMap, singeDomanList.get(0) + "." + singeDomanList.get(1), singleDomanCount);
                putOrAdd(resultMap, singeDomanList.get(1), singleDomanCount);
            }

            if (size == 3) {
                putOrAdd(resultMap, singeDomanList.get(0) + "." + singeDomanList.get(1) + "." + singeDomanList.get(2), singleDomanCount);
                putOrAdd(resultMap, singeDomanList.get(1) + "." + singeDomanList.get(2), singleDomanCount);
                putOrAdd(resultMap, singeDomanList.get(2), singleDomanCount);
            }
        }
        List<String> resultList = new ArrayList<>();
        Set<Map.Entry<String, Integer>> entries = resultMap.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(entry.getValue()).append(" ").append(entry.getKey());
            resultList.add(stringBuilder.toString());
        }
        return resultList;

    }


    public void putOrAdd(Map<String, Integer> map, String key, int count) {
        if (map.containsKey(key)) {
            map.put(key, map.get(key) + count);
        } else {
            map.put(key, count);
        }
    }


    public static void main(String[] args) {
        String[] domains = new String[]{"900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"};
        List<String> strings = new 子域名访问计数().subdomainVisits(domains);
        System.out.println(strings);
    }

}
