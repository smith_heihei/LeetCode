package easy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HomorSmith on 2018/6/1.
 */
public class 字符的最短距离 {

    public int[] shortestToChar(String S, char C) {

        int[] posArr = new int[S.length()];

        List<Integer> posList = new ArrayList<>();
        for (int i = 0; i < S.length(); i++) {
            char c = S.charAt(i);
            if (c == C) {
                posList.add(i);
            }
        }

        int posIndex = 0;
        for (int i = 0; i < S.length(); i++) {
            if (i <= posList.get(0)) {
                posArr[i] = posList.get(0) - i;
            }
            if (i >= posList.get(posList.size() - 1)) {
                posArr[i] = i - posList.get(posList.size()-1);
            }

            if (i > posList.get(posIndex) && i < posList.get(posList.size() - 1)) {
                Integer pre = posList.get(posIndex);
                Integer next = posList.get(posIndex + 1);
                posArr[i] = Math.abs(i - pre) > Math.abs(next - i) ?  Math.abs(next - i):Math.abs(i - pre) ;
                if (i >= next) {
                    posIndex++;
                }
            }

        }

        return posArr;
    }

    public static void main(String[] args) {
        int[] ints = new 字符的最短距离().shortestToChar("loveleetcode", 'e');
        for (int i = 0; i < ints.length; i++) {
            System.out.print(ints[i]+"\t");
        }
    }

}
