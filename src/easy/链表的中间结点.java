package easy;

import train.linkedlist.ListNode;

/**
 * @atuhor :eavawu
 * time:2018/8/3
 * todo:
 */
public class 链表的中间结点 {


    public ListNode middleNode(ListNode head) {
        ListNode tempNode = head;
        int len = 0;
        while (tempNode != null) {
            tempNode = tempNode.next;
            len++;
        }
        int middle = len % 2 == 0 ?len / 2 + 1:(len+1) / 2;
        len = 0;
        tempNode = head;
      //  System.out.printf("middle = "+middle);
        while (len != middle-1) {
            tempNode = tempNode.next;
            len++;
        }
    //    System.out.printf("middle = "+tempNode.val);
        return tempNode;
    }


    public static void main(String[] args) {
        ListNode listNode = ListNode.create(new int[]{1, 2, 3, 4, 5, 6, 7});
        listNode = new 链表的中间结点().middleNode(listNode);
        listNode.printListNode(listNode);
    }
}

