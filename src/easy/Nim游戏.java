package easy;

/**
 * Created by HomorSmith on 2018/5/24.
 */
public class Nim游戏 {
    /**
     * 你和你的朋友，两个人一起玩Nim游戏：桌子上有一堆石头，每次你们轮流拿掉 1 - 3 块石头。 拿掉最后一块石头的人就是获胜者。你作为先手。
     * <p>
     * 你们是聪明人，每一步都是最优解。 编写一个函数，来判断你是否可以在给定石头数量的情况下赢得游戏。
     * <p>
     * 示例:
     * <p>
     * 输入: 4
     * 输出: false
     * 解释: 如果堆中有 4 块石头，那么你永远不会赢得比赛；
     * 因为无论你拿走 1块、2块 还是 3块石头，最后一块石头总是会被你的朋友拿走。
     */
    public boolean canWinNim(int n) {
        if (n <= 3) {

            return true;
        }
        return n%(3+1)!=0;
    }

    public static void main(String[] args) {
        Nim游戏 nim = new Nim游戏();
        boolean b = nim.canWinNim(4);
        String resultStr = b ? "赢了" : "输了";
        System.out.println(resultStr);
    }

}
