package easy;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by HomorSmith on 2018/6/15.
 */
public class 旋转数字 {
    public int rotatedDigits(int N) {
        Map<Character, Boolean> map = new HashMap<>();
        map.put('0', true);
        map.put('1', true);
        map.put('2', true);
        map.put('3', false);
        map.put('4', false);
        map.put('5', true);
        map.put('6', true);
        map.put('7', false);
        map.put('8', true);
        map.put('9', true);

        int count = 0;
        for (int i = 0; i < N; i++) {
            StringBuilder srcBuilder = new StringBuilder();
            srcBuilder.append(i);
            String rightDirStr = srcBuilder.toString();
            String reverse = srcBuilder.reverse().toString();
            boolean isGoodNum = true;
//            System.out.println("reverse = "+rightDirStr);
            for (int j = 0; j < reverse.length(); j++) {
//                System.out.printf(map.get(reverse.charAt(j))+"hhh");
                if (!map.get(reverse.charAt(j))) {
                    isGoodNum = false;
                    break;
                }
                if (reverse.length() == 1&&(reverse.charAt(j)== '8'||reverse.charAt(j) == '0'||reverse.charAt(j) == '1')){
                    isGoodNum = false;
                    break;
                }
                if (reverse.length() == 2&& reverse.equals("10")){
                    isGoodNum = false;
                    break;
                }
                if (reverse.length() >1&&reverse.equals(rightDirStr)){
                    isGoodNum = false;
                    break;
                }

            }
            if (isGoodNum) {
                System.out.println("good num = "+i);
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        int i = new 旋转数字().rotatedDigits(10);
    }


}
