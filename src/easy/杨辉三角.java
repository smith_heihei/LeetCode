package easy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HomorSmith on 2018/6/6.
 */
public class 杨辉三角 {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> resultList = new ArrayList<>();
        for (int i = 0; i < numRows; i++) {
            List<Integer> tempList = new ArrayList<>();

            for (int j = 0; j <= i; j++) {
                if (j == 0 || i < 1 || j == i) {
                    tempList.add(1);
                } else {
                    tempList.add(resultList.get(i - 1).get(j - 1) + resultList.get(i- 1).get(j));
                }
            }
            resultList.add(tempList);
            System.out.printf("tempList = "+tempList.toString());

        }
        return resultList;
    }

    public static void main(String[] args) {
        List<List<Integer>> generate = new 杨辉三角().generate(5);
        for (int i = 0; i < generate.size(); i++) {
            for (int j = 0; j < generate.get(i).size(); j++) {
                Integer integer = generate.get(i).get(j);
                System.out.printf(integer + "");
            }
            System.out.println();
        }
    }

}
