package easy;

/**
 * @atuhor :eavawu
 * time:2018/5/31
 * todo:
 */
public class 将有序数组转换为二叉搜索树 {
    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }


    public TreeNode sortedArrayToBST(int[] nums) {
        return sortedArrayToBST(nums, 0, nums.length - 1);
    }


    private TreeNode sortedArrayToBST(int[] nums, int start, int end) {
        if (start > end) return null;
        int mid = (end + start) / 2;
        TreeNode root = new TreeNode(nums[mid]);
        root.left = sortedArrayToBST(nums, start, mid - 1);
        root.right = sortedArrayToBST(nums, mid + 1, end);
        return root;
    }

    public static void preOrder(TreeNode treeNode){
        if (treeNode==null){
            return;
        }
        System.out.println(treeNode.val);
        preOrder(treeNode.left);
        preOrder(treeNode.right);

    }


    public static void main(String[] args) {
        TreeNode treeNode = new 将有序数组转换为二叉搜索树().sortedArrayToBST(new int[]{-10,-3,0,5,9});
        preOrder(treeNode);
    }


    /**
     * struct node* sortedArrayToBST(int arr[], int start, int end)
     {
     if (start > end) return NULL;
     // 这里同（start+left）/2，目的是为了防止溢出.
     int mid = start + (end - start) / 2;
     struct node *root = newNode(arr[mid]);//newNode创建二叉树结点，具体代码请看文章 二叉树问题汇总（1）
     root->left = sortedArrayToBST(arr, start, mid-1);
     root->right = sortedArrayToBST(arr, mid+1, end);
     return root;
     }

     struct node* sortedArrayToBST(int arr[], int n)
     {
     return sortedArrayToBST(arr, 0, n-1);

     */

}
