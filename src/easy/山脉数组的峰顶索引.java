package easy;

/**
 * Created by HomorSmith on 2018/7/6.
 */
public class 山脉数组的峰顶索引 {

    public int peakIndexInMountainArray(int[] A) {
        if (A.length==0){
            return 0;
        }
        if (A.length == 1){
            return A[0];
        }
        int max = A[0];
        int index = 0;
        for (int i = 0; i < A.length; i++) {
            if (max<A[i]){
                max = A[i];
                index = i;
            }
        }
        return index;
    }

}
