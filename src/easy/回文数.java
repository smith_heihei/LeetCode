package easy;

/**
 * Created by HomorSmith on 2018/6/7.
 */
public class 回文数 {
    public boolean isPalindrome(int x) {
        String  numStr = x+"";
        int offset = numStr.length()%2==0?0:1;
        String left = numStr.substring(0,numStr.length()/2);
        String right =new StringBuilder(numStr.substring(numStr.length()/2+offset,numStr.length())).reverse().toString();
        return left.equals(right);

    }

    public static void main(String[] args) {
        System.out.println(new 回文数().isPalindrome(12321));
    }

}
