package easy;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @atuhor :eavawu
 * time:2018/7/5
 * todo:
 */
public class 左叶子之和 {
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }


    /* Function to insert data recursively */
    private void insert(TreeNode node, int data) {
        TreeNode newNode = null;
        if (node.left == null) {
            node.left = new TreeNode(data);
            return;
        } else if (node.right == null) {
            node.right = new TreeNode(data);
            return;
        }
        if (node != null) {
            if (data > node.val) {
                insert(node.right, data);
            } else {
                insert(node.left, data);
            }
        }
    }


    public int sumOfLeftLeaves(TreeNode root) {
        int sum = 0;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            TreeNode poll = queue.poll();
            if (poll.left != null) {
                queue.offer(poll.left);
                if (poll.left.left == null && poll.left.right == null) {
                    sum = sum + poll.left.val;
                }
            }
            if (poll.right != null) {
                queue.offer(poll.right);
            }

        }
        return sum;
    }


    public void preOrder(TreeNode root) {
        if (root != null) {
            System.out.println(root.val + "");
            preOrder(root.left);
            preOrder(root.right);
        }
    }





    public static void main(String[] args) {
        TreeNode rootNode = new TreeNode(5);
        TreeNode tempRoot = rootNode;
        左叶子之和 obj = new 左叶子之和();
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                continue;
            }
            obj.insert(tempRoot, i);
        }
        tempRoot = rootNode;

        obj.preOrder(tempRoot);

        tempRoot = rootNode;





        int i = obj.sumOfLeftLeaves(tempRoot);
        System.out.printf("result = "+i);
    }

}
