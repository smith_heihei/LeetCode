package easy;

/**
 * Created by HomorSmith on 2018/7/9.
 */
public class 第一个错误的版本 {
    boolean isBadVersion(int version) {
        if (version == 3) {
            return true;
        }
        return false;
    }

    public int firstBadVersion(int n) {

        int start = 0;
        int end = n;
        int middle = 0;
        while (end > start) {
            middle = (start + end) / 2;
            if (isBadVersion(middle)) {
                end = middle ;
            } else{
                start = middle + 1;
            }

        }
        return start;
    }
}
