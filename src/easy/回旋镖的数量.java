package easy;

/**
 * Created by HomorSmith on 2018/6/14.
 */
public class 回旋镖的数量 {
    public int numberOfBoomerangs(int[][] points) {
        int count = 0;
        for (int i = 0; i < points.length; i++) {
            for (int j = i+1; j < points.length; j++) {
                for (int k = j+1; k < points.length; k++) {
                  if (calDistance(points[i],points[j] ) == calDistance(points[i],points[k] )) {
                      count++;
                  }

                    if (calDistance(points[j],points[i] ) == calDistance(points[j],points[k] )) {
                        count++;
                    }

                    if (calDistance(points[k],points[i] ) == calDistance(points[j],points[k] )) {
                        count++;
                    }

                }
            }
        }
        return count;
    }


    public int calDistance(int[] p1, int[] p2) {
        int dx = Math.abs(p2[0] - p1[0]);
        int dy = Math.abs(p2[1] - p1[1]);
        return dx*dx + dy*dy;
    }

    public static void main(String[] args) {
        int[][] param = new int[][]{{0,0},{1,0},{2,0}};
        int i = new 回旋镖的数量().numberOfBoomerangs(param);
        System.out.println("result is "+i);
    }
}
