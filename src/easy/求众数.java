package easy;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by HomorSmith on 2018/6/11.
 */
public class 求众数 {
    public int majorityElement(int[] nums) {
        if (nums.length==1){
            return nums[0];
        }
        Map<Integer,Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])){
               int count =  map.get(nums[i])+1;
               if (count>nums.length/2){
                   return nums[i];
               }
                map.put(nums[i],count);
            }else{
                map.put(nums[i],1);
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        int i = new 求众数().majorityElement(new int[]{3, 2, 3});
        System.out.printf(i+"");
    }
}
