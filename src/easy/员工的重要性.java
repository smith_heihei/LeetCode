package easy;

import java.util.*;

/**
 * Created by HomorSmith on 2018/6/14.
 */
public class 员工的重要性 {
   static class Employee {
        // It's the unique id of each node;
        // unique id of this employee
        public int id;
        // the importance value of this employee
        public int importance;
        // the id of direct subordinates
        public List<Integer> subordinates;

       @Override
       public String toString() {
           return "Employee{" +
                   "id=" + id +
                   ", importance=" + importance +
                   ", subordinates=" + subordinates +
                   '}';
       }
   };

    public int getImportance(List<Employee> employees, int id) {
        Map<Integer,Employee> map = new HashMap<>();
        Employee parentEmployee = null;
        for (int i = 0; i < employees.size(); i++) {
            Employee employee =  employees.get(i);
            if (employees.get(i).id == id){
                parentEmployee = employee;
            }
            map.put(employee.id,employee);
        }
        int result =0;
        List<Integer> subordinates = parentEmployee.subordinates;

        Queue<Employee> queue = new LinkedList();
        queue.offer(parentEmployee);
        while (!queue.isEmpty()){
            Employee poll = queue.poll();
            if (poll == null){
                continue;
            }
            System.out.println(poll.toString());
            result = result+poll.importance;
            for (int i = 0; i <poll.subordinates.size(); i++) {
//                System.out.println(poll.subordinates.get(i));
//                System.out.println(map.get(poll.subordinates.get(i)).toString());
                queue.offer(map.get(poll.subordinates.get(i)));

            }
        }
        return result;
    };

    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();
        Employee employee = new Employee();
        employee.importance = 5;
        employee.id = 1;
        employee.subordinates = new ArrayList<>();
        employee.subordinates.add(2);
        employee.subordinates.add(3);
        employees.add(employee);


        employee = new Employee();
        employee.importance = 3;
        employee.id = 2;
        employee.subordinates = new ArrayList<>();
        employees.add(employee);


        employee = new Employee();
        employee.importance = 3;
        employee.id = 3;
        employee.subordinates = new ArrayList<>();
        employees.add(employee);


        int importance = new 员工的重要性().getImportance(employees, 1);
        System.out.println("result = "+importance);
    }
}
