package easy;

/**
 * Created by HomorSmith on 2018/6/15.
 */
public class 二叉树的坡度 {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
    int sum(TreeNode treeNode, int total) {
        if (treeNode == null) {
            return 0;
        }
        int leftSum =   sum(treeNode.left, total);
        int rightSum =  sum(treeNode.right, total);
        total = total+Math.abs(leftSum - rightSum);
        return total;
    }



    public int findTilt(TreeNode root) {
        if (root==null){
            return 0;
        }
        if (root.left == null&&root.right == null){
            return 0;
        }
        int left = sum(root.left, 0);
        int right = sum(root.right,0);
        System.out.printf("left = "+left);
        System.out.printf("right = "+right);
        return Math.abs(left-right);
    }
}
