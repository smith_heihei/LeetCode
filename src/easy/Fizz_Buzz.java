package easy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HomorSmith on 2018/6/4.
 */
public class Fizz_Buzz {
    public List<String> fizzBuzz(int n) {
        List<String> resultList = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            if (i%3==0&&i%5==0){
                resultList.add("FizzBuzz");
            }else if (i%3 ==0 ){
                resultList.add("Fizz");
            }else if(i%5==0){
                resultList.add("Buzz");
            }else{
                resultList.add(i+"");
            }
        }
        return resultList;
    }

    public static void main(String[] args) {
        List<String> strings = new Fizz_Buzz().fizzBuzz(15);
        System.out.println(strings);
    }
}
