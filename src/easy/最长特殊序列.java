package easy;

/**
 * Created by HomorSmith on 2018/6/6.
 */
public class 最长特殊序列 {
    public int findLUSlength(String a, String b) {
        int lenA = a.length();
        int lenB = b.length();
        if(lenA != lenB)
            return Math.max(lenA,lenB);
        else{
            if(a.equals(b))
                return -1;
            else
                return lenA;
        }
    }
}
