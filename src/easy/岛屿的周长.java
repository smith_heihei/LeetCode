package easy;

/**
 * @atuhor :eavawu
 * time:2018/5/31
 * todo:
 */
public class 岛屿的周长 {
    public int islandPerimeter(int[][] grid) {
        int aoundLen = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                int unit = grid[i][j];
                if (unit == 0) {
                    continue;
                }
                if (unit == 1) {
                    aoundLen = aoundLen + 4;
                }
                if (i > 0 && grid[i - 1][j] == 1) {
                    aoundLen = aoundLen - 2;
                }
                if (j > 0 && grid[i][j - 1] == 1) {
                    aoundLen = aoundLen - 2;
                }
            }
        }
        return aoundLen;
    }

    public static void main(String[] args) {
        int[][] arrs = new int[][]{{0, 1, 0, 0},
                {1, 1, 1, 0},
                {0, 1, 0, 0},
                {1, 1, 0, 0}};
        int i = new 岛屿的周长().islandPerimeter(arrs);
        System.out.println("长度 = "+i);
    }
}
