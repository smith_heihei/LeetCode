package easy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HomorSmith on 2018/5/30.
 */
public class 反转字符串中的单词_III {
    public String reverseWords(String s) {
        String[] split = s.split(" ");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < split.length; i++) {
            String reverseWord = reverseWord(split[i]);
            if (split.length -1 == i){
                stringBuilder.append(reverseWord);
            }else{
                stringBuilder.append(reverseWord).append(" ");

            }
        }
        return stringBuilder.toString();
    }

    ;

    public String reverseWord(String word) {
        char[] chars = word.toCharArray();
        for (int i = 0; i < word.length() / 2; i++) {
            char tempChar = chars[i];
            chars[i] = chars[word.length() - 1 - i];
            chars[word.length() - 1 - i] = tempChar;
        }
        return new String(chars);
    }


    public static void main(String[] args) {
        反转字符串中的单词_III reverseWord = new 反转字符串中的单词_III();
        String reverseWordStr = reverseWord.reverseWords("Let's take LeetCode contest hello");
        System.out.println(reverseWordStr);
    }

}
