package easy;

/**
 * Created by HomorSmith on 2018/5/23.
 */

import java.util.ArrayList;
import java.util.List;

/**
 * 自除数 是指可以被它包含的每一位数除尽的数。
 * <p>
 * 例如，128 是一个自除数，因为 128 % 1 == 0，128 % 2 == 0，128 % 8 == 0。
 * <p>
 * 还有，自除数不允许包含 0 。
 * <p>
 * 给定上边界和下边界数字，输出一个列表，列表的元素是边界（含边界）内所有的自除数。
 */
public class 自除数 {
    public List<Integer> selfDividingNumbers(int left, int right) {
        List<Integer> list = new ArrayList<>();
        for (int i = left; i <=right; i++) {
            if (i==0){
                continue;
            }
            boolean dividingNumber = isDividingNumber(i);
            if (dividingNumber) {
                list.add(i);
            }
        }
        return list;
    }

    public boolean isDividingNumber(int number) {
        List<Integer> digitList = new ArrayList<>();
        int count = 0;
        int temp = number;
        //得出位数
        while (number != 0) {
            digitList.add(0,number % 10);
            number = number / 10;
            count++;
        }
        for (int i = 0; i < count; i++) {
            if (digitList.get(i)==0){
                return false;
            }
            int divisiResult = temp % digitList.get(i);
            if (divisiResult != 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        自除数 selfDivide = new 自除数();
        List<Integer> list = selfDivide.selfDividingNumbers(1, 22);
        System.out.println(list);
    }
}
