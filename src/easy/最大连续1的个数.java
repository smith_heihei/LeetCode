package easy;

/**
 * Created by HomorSmith on 2018/6/6.
 */
public class 最大连续1的个数 {
    public int findMaxConsecutiveOnes(int[] nums) {
        int maxCount = 0;
        int curCount = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 1) {
                curCount++;
            }
            if (nums[i] == 0||nums.length-1 == i ) {
                if (maxCount < curCount) {
                    maxCount = curCount;
                }
                curCount = 0;
            }
        }
        return maxCount;
    }

    public static void main(String[] args) {
        int[] intArr = new int[]{1, 1, 0, 1, 1, 1};
        int maxConsecutiveOnes = new 最大连续1的个数().findMaxConsecutiveOnes(intArr);
        System.out.printf(maxConsecutiveOnes + "\t");
    }
}
